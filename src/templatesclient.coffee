# templatesclient.coffee
# Copyright 2014-2016 9165584 Canada Corporation <legal@fuzzy.ai>
# All rights reserved.

MicroserviceClient = require '@fuzzy-ai/microservice-client'

class TemplatesClient extends MicroserviceClient

  templates: (callback) ->
    @get "/templates", callback

module.exports = TemplatesClient
