# httperror.coffee
# Copyright 2014 9165584 Canada Corporation <legal@fuzzy.ai>
# All rights reserved.

class HTTPError extends Error
  constructor: (@message, @statusCode) ->

module.exports = HTTPError
