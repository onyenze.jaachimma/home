# src/actions/stats.coffee
# Copyright 2016 9165584 Canada Corporation <legal@fuzzy.ai>
# All rights reserved.

require 'es6-promise'
_ = require 'lodash'
fetch = require 'isomorphic-fetch'
{ push } = require 'react-router-redux'
FetchError = require '../fetch-error'

lpad = (value, padding) ->
  zeroes = "0"
  zeroes += "0" for i in [1..padding]
  (zeroes + value).slice(padding * -1)

requestStats = (year, month) ->
  type: 'REQUEST_STATS'
  year: year
  month: month

receiveStats = (year, month, total, byDay) ->
  type: 'RECEIVE_STATS'
  year: year
  month: month
  total: total
  byDay: byDay

statsError = (year, month, err) ->
  type: 'STATS_ERROR'
  year: year
  month: month
  error: err

requestAgentStats = (year, month, agentID) ->
  type: 'REQUEST_AGENT_STATS'
  year: year
  month: month
  agentID: agentID

exports.fetchStats = (year, month) ->
  (dispatch) ->
    dispatch(requestStats(year, month))
    fetch("/data/stats/user/#{lpad(year, 4)}/#{lpad(month, 2)}",
      credentials: 'same-origin',
      headers:
        'Accept': 'application/json'
        'Content-Type': 'application/json'
    )
    .then (response) ->
      response.json()
      .then (json) ->
        if response.ok
          dispatch receiveStats year, month, json.total, json.byDay
        else
          err = new FetchError response, json
          dispatch statsError year, month, err
      .catch (err) ->
        dispatch statsError year, month, err
    .catch (err) ->
      dispatch statsError year, month, err

receiveAgentStats = (year, month, agentID, total, byDay) ->
  type: 'RECEIVE_AGENT_STATS'
  year: year
  month: month
  total: total
  byDay: byDay
  agentID: agentID

agentStatsError = (year, month, agentID, err) ->
  type: 'AGENT_STATS_ERROR'
  year: year
  month: month
  error: err
  agentID: agentID

exports.fetchAgentStats = (year, month, agentID) ->
  (dispatch) ->
    dispatch requestAgentStats year, month, agentID
    fetch "/data/stats/agent/#{agentID}/#{lpad(year, 4)}/#{lpad(month, 2)}",
      credentials: 'same-origin',
      headers:
        'Accept': 'application/json'
        'Content-Type': 'application/json'
    .then (response) ->
      response.json()
      .then (json) ->
        if response.ok
          dispatch receiveAgentStats year, month, agentID, json.total, json.byDay
        else
          err = new FetchError response, json
          dispatch agentStatsError year, month, agentID, err
      .catch (err) ->
        dispatch agentStatsError year, month, agentID, err
    .catch (err) ->
      dispatch agentStatsError year, month, agentID, err
