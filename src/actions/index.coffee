# src/actions/index.coffee
# Copyright 2016 9165584 Canada Corporation <legal@fuzzy.ai>
# All rights reserved.

FetchError = require '../fetch-error'

{ push } = require 'react-router-redux'

exports.checkResponse = (response, dispatch) ->
  if response.status >= 200 and response.status < 300
    return response.json()
  else if response.status == 401
    { logOut } = require './user'
    dispatch(logOut())
  else if response.status == 404
    dispatch(push('/not-found'))
  else
    response.json()
      .then (json) ->
        throw new FetchError(response, json)
      .catch (err) ->
        throw new FetchError(response, err)
