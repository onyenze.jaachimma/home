require 'es6-promise'
fetch = require 'isomorphic-fetch'
{ push } = require 'react-router-redux'

{ checkResponse } = require '.'
FetchError = require '../fetch-error'

exports.reset = ->
  type: 'USER_RESET'

beginLogin = ->
  return { type: 'LOGIN_USER' }

loginSuccess = (user) ->
  type: 'LOGIN_USER_SUCCESS'
  user: user

loginError = (err, needsReConfirm) ->
  type: 'LOGIN_USER_ERROR'
  error: err
  needsReConfirm: needsReConfirm

exports.logIn = (data) ->
  (dispatch) ->
    dispatch beginLogin()

    fetch('/login',
      method: 'post'
      credentials: 'same-origin',
      headers:
        'Accept': 'application/json'
        'Content-Type': 'application/json'
        'X-CSRF-Token': window.CSRF_TOKEN
      body: JSON.stringify data)
    .then (response) ->
      if response.status >= 200 and response.status < 300
        return response.json()
      else
        response.json()
          .then (json) ->
            throw new FetchError(response, json)
          .catch (err) ->
            throw new FetchError(response, err)
    .then (data) ->
      if data.status == 'OK'
        dispatch loginSuccess(data.user)
        dispatch push(data.url)
      else
        needsReConfirm = false
        if data.message == "This account needs to be confirmed."
          needsReConfirm = true
        dispatch loginError(data, needsReConfirm)
    .catch (err) ->
      dispatch loginError(err, false)

beginRequest = ->
  return { type: 'REQUEST_INVITE' }

requestSuccess = (message) ->
  return {type: 'REQUEST_INVITE_SUCCESS'}

requestError = (message) ->
  type: 'REQUEST_INVITE_ERROR'
  message: message

exports.requestInvite = (data) ->
  (dispatch) ->
    dispatch(beginRequest())

    fetch('/request',
      method: 'post'
      credentials: 'same-origin',
      headers:
        'Accept': 'application/json'
        'Content-Type': 'application/json'
        'X-CSRF-Token': window.CSRF_TOKEN
      body: JSON.stringify data
    ).then((response) ->
      if response.status == 200
        dispatch(requestSuccess())
        dispatch(push('/request-complete'))
        return
      else
        return response.json()
    ).then((data) ->
      if data
        dispatch(requestError(data.message))
    )

beginSignup = ->
  return { type: 'SIGNUP'}

signupSuccess = ->
  return { type: 'SIGNUP_SUCESS' }

signupError = (message, needsReConfirm) ->
  type: 'SIGNUP_ERROR'
  message: message
  needsReConfirm: needsReConfirm

exports.signup = (data) ->
  (dispatch) ->
    dispatch(beginSignup())

    fetch('/signup',
      method: 'post'
      credentials: 'same-origin',
      headers:
        'Accept': 'application/json'
        'Content-Type': 'application/json'
        'X-CSRF-Token': window.CSRF_TOKEN
      body: JSON.stringify data
    ).then((response) ->
      if response.status == 200
        window?.Intercom? 'trackEvent', 'user-signup',
        window?.ga? 'send',
          hitType: 'event'
          eventCategory: 'User'
          eventAction: 'Sign up'
        dispatch(signupSuccess())
        dispatch(push('/signup-complete'))
        return
      else
        return response.json()
    ).then((data) ->
      if data
        needsReConfirm = false
        if data.message.match /.*(confirmed).*/
          needsReConfirm = true
        dispatch(signupError(data.message, needsReConfirm))
    )

exports.setUser = (user) ->
  type: 'USER_SET'
  user: user

beginLogout = ->
  return { type: 'LOGOUT_USER'}

logoutSuccess = ->
  return { type: 'LOGOUT_USER_SUCCESS'}

logoutError = ->
  return { type: 'LOGOUT_USER_ERROR'}

exports.logOut = ->
  (dispatch) ->
    dispatch(beginLogout())

    fetch('/logout',
      method: 'post'
      credentials: 'same-origin',
      headers:
        'Accept': 'application/json'
        'Content-Type': 'application/json'
        'X-CSRF-Token': window.CSRF_TOKEN)
    .then (response) ->
      if response.status == 200
        dispatch logoutSuccess()
        dispatch push('/login')
      else
        dispatch logoutError()

beginResendConfirm = ->
  type: 'RESEND_CONFIRMATION'

resendConfirmationSuccess = (message) ->
  type: 'RESEND_CONFIRMATION_SUCCESS'
  message: message

resendConfirmationError = (message) ->
  type: 'RESEND_CONFIRMATION_ERROR'
  message: message

exports.resendConfirmation = (data) ->
  (dispatch) ->
    dispatch(beginResendConfirm())

    fetch('/resend-confirmation',
      method: 'post'
      credentials: 'same-origin'
      headers:
        'Accept': 'application/json'
        'Content-Type': 'application/json'
        'X-CSRF-Token': window.CSRF_TOKEN
      body: JSON.stringify data
    ).then((response) ->
      response.json()
    ).then((json) ->
      if json.status == 'OK'
        json.message = "Check your email to confirm your account."
        dispatch(resendConfirmationSuccess(json.message))
      else
        dispatch(resendConfirmationError(json.message))
    )

beginUpdateUser = ->
  type: 'UPDATE_USER'

updateUserSuccess = (user) ->
  type: 'UPDATE_USER_SUCCESS'
  user: user

updateUserError = (err) ->
  type: 'UPDATE_USER_ERROR'
  error: err

exports.updateUser = (data) ->
  (dispatch) ->
    dispatch(beginUpdateUser())

    fetch('/data/user',
      method: 'put'
      credentials: 'same-origin'
      headers:
        'Accept': 'application/json'
        'Content-Type': 'application/json'
        'X-CSRF-Token': window.CSRF_TOKEN
      body: JSON.stringify data)
    .then (response) -> checkResponse(response, dispatch)
    .then (json) ->
      dispatch updateUserSuccess(json.user)
    .catch (err) ->
      dispatch updateUserError(err)

beginChangePassword = ->
  type: 'CHANGE_PASSWORD'

changePasswordSuccess = (user) ->
  type: 'CHANGE_PASSWORD_SUCCESS'
  user: user

changePasswordError = (err) ->
  type: 'CHANGE_PASSWORD_ERROR'
  error: err

exports.changePassword = (data) ->
  (dispatch) ->
    dispatch beginChangePassword()
    fetch('/data/change-password',
      method: 'post'
      credentials: 'same-origin'
      headers:
        'Accept': 'application/json'
        'Content-Type': 'application/json'
        'X-CSRF-Token': window.CSRF_TOKEN
      body: JSON.stringify data)
    .then (response) -> checkResponse(response, dispatch)
    .then (json) ->
      dispatch changePasswordSuccess(json.user)
    .catch (err) ->
      dispatch changePasswordError(err)

beginResetRequest = ->
  type: 'RESET_REQUEST'

resetRequestSuccess = ->
  type: 'RESET_REQUEST_SUCCESS'

resetRequestError = (err) ->
  type: 'RESET_REQUEST_ERROR'
  error: err

exports.resetRequest = (data) ->
  (dispatch) ->
    dispatch beginResetRequest()

    fetch('/reset'
      method: 'post'
      credentials: 'same-origin'
      headers:
        'Accept': 'application/json'
        'Content-Type': 'application/json'
        'X-CSRF-Token': window.CSRF_TOKEN
      body: JSON.stringify data)
    .then (response) -> checkResponse(response, dispatch)
    .then (json) ->
      dispatch resetRequestSuccess()
      dispatch push('/reset-requested')
    .catch (err) ->
      dispatch resetPasswordError(err)

beginResetPassword = ->
  type: 'RESET_PASSWORD'

resetPasswordSuccess = ->
  type: 'RESET_PASSWORD_SUCCESS'

resetPasswordError = (err) ->
  type: 'RESET_PASSWORD_ERROR'
  error: err

exports.resetPassword = (data) ->
  (dispatch) ->
    dispatch beginResetPassword()

    fetch('/change-password'
      method: 'post'
      credentials: 'same-origin'
      headers:
        'Accept': 'application/json'
        'Content-Type': 'application/json'
        'X-CSRF-Token': window.CSRF_TOKEN
      body: JSON.stringify data)
    .then (response) -> response.json()
    .then (json) ->
      if json.status == 'OK'
        dispatch resetPasswordSuccess()
        dispatch push('/password-changed')
      else
        dispatch(resetPasswordError(json))
    .catch (err) ->
      dispatch resetPasswordError(json)
