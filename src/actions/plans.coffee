require 'es6-promise'
fetch = require 'isomorphic-fetch'

FetchError = require '../fetch-error'

# @TODO : should probably abstract the fetch pattern
BASE_URL = ''
if not window?
  BASE_URL = process.env['BASE_URL'] || "http://localhost:8000"

requestPlans = ->
  return {type: 'REQUEST_PLANS'}

receivePlans = (plans) ->
  type: 'RECEIVE_PLANS'
  plans: plans

requestPlansError = (err) ->
  type: 'REQUEST_PLANS_ERROR'
  error: err

exports.fetchPlans = ->
  (dispatch) ->
    dispatch(requestPlans())
    fetch("#{BASE_URL}/data/plans",
      method: 'get'
      credentials: 'same-origin'
      headers:
        'Accept': 'application/json'
        'Content-Type': 'application/json'
    )
    .then (response) ->
      response.json()
      .then (json) ->
        if response.ok
          dispatch receivePlans json
        else
          err = new FetchError response, json
          dispatch requestPlansError err
      .catch (err) ->
        dispatch requestPlansError err
    .catch (err) ->
      dispatch requestPlansError err

# XXX: These look a lot like the functions above sorrynotsorry

requestPlan = (stripeID) ->
  type: 'REQUEST_PLAN'
  stripeID: stripeID

receivePlan = (plan) ->
  type: 'RECEIVE_PLAN'
  plan: plan

requestPlanError = (stripeID, err) ->
  type: 'REQUEST_PLAN_ERROR'
  stripeID: stripeID
  error: err

exports.fetchPlan = (stripeID) ->
  (dispatch) ->
    dispatch(requestPlan(stripeID))
    fetch("#{BASE_URL}/data/plan/#{stripeID}",
      method: 'get'
      credentials: 'same-origin'
      headers:
        'Accept': 'application/json'
        'Content-Type': 'application/json'
    ).then (response) ->
      response.json()
      .then (json) ->
        if response.ok
          dispatch receivePlan json
        else
          err = new FetchError response, json
          dispatch requestPlanError stripeID, err
      .catch (err) ->
        dispatch requestPlanError stripeID, err
    .catch (err) ->
      dispatch requestPlanError stripeID, err
