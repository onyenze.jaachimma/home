_ = require 'lodash'
React = require 'react'
Link = require 'react-router/lib/Link'
{ connect } = require 'react-redux'
{ DeleteCross } = require './icons'

PerformanceMetric = React.createClass

  handleDelete: (e) ->
    e.preventDefault()
    @props.deleteHandler(@props.name)

  displayName: 'PerformanceMetric'

  # coffeelint: disable=max_line_length
  render: ->
    {name, value} = @props
    <div className="performance-metric-unit">
      <span className="performance-metric-unit__name">{name}:</span>
      <span className="performance-metric-unit__value">{value}</span>
      <button onClick={@handleDelete} className="performance-metric-unit__delete-icon">
        <DeleteCross />
      </button>
    </div>

module.exports = PerformanceMetric
