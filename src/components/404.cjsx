React = require 'react'

module.exports = React.createClass
  displayName: "FourOhFour"

  # coffeelint: disable=max_line_length
  render: ->
    <div className="row">
      <div className="page">
        <h1 className="fwb four-oh-four">404</h1>
        <p>We can&rsquo;t seem to find the page you are looking for</p>
      </div>
    </div>
