_ = require 'lodash'
React = require 'react'

RangeInput = React.createClass
  displayName: 'RangeInput'

  getInitialState: ->
    fromValue: @props.fromValue
    toValue: @props.toValue
    errors: {}

  handleChange: (e) ->
    errors = {}

    fromValue = (@refs.from.value - 0)
    if not _.isFinite(fromValue)
      errors.from = 'Must be a number'

    toValue = (@refs.to.value - 0)
    if not _.isFinite(toValue)
      errors.to = 'Must be a number'

    if fromValue > toValue
      errors.to = 'Must be greater than from value'

    @setState
      fromValue: @refs.from.value
      toValue: @refs.to.value
      errors: errors

    if _.size(errors) == 0
      @props.handleRange [fromValue , toValue]

  # coffeelint: disable=max_line_length
  render: ->
    <div className="blank-agent-range-inputs">
      <div className="blank-agent-range-inputs__wrap">
        <div className="range-input range-input-from">
          <span className="label">From:</span>
          <input ref="from" id="from" value={@state.fromValue} type="text"  onChange={@handleChange} className="range-input" />
          {if @state.errors?.from
            <label htmlFor="from" className="error">{@state.errors.from}</label>
          }
        </div>
        <div className="range-input range-input-to">
          <span className="label">To:</span>
          <input ref="to" id="to" value={@state.toValue} type="text" onChange={@handleChange} className="range-input" />
          {if @state.errors?.to
            <label htmlFor="to" className="error">{@state.errors.to}</label>
          }
        </div>
      </div>
    </div>

module.exports = RangeInput
