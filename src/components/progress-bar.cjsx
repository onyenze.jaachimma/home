React = require 'react'

ProgressBar = React.createClass

  displayName: "ProgressBar"

  # coffeelint: disable=max_line_length
  render: ->
    {inProgress} = @props

    <div className="progress-bar" data-in-progress="#{inProgress}">
      { if inProgress
        <div className="agent__inprogress">
          <div className="loading">
            <div className="loading-bar"></div>
            <div className="loading-bar"></div>
            <div className="loading-bar"></div>
            <div className="loading-bar"></div>
          </div>
        </div>
      }
    </div>

module.exports = ProgressBar
