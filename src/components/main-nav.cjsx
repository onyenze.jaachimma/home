React = require 'react'
{ Link } = require 'react-router'


module.exports = React.createClass
  displayName: "Main logged In Nav"

  render: ->

    <ul className="navigation">
      <li className="navigation__item">
        <Link to="/" className="navigation__link navigation__link__active">Home</Link>
      </li>
      <li className="navigation__item">
        <Link to="/how-it-works" className="navigation__link">How it works</Link>
      </li>
      <li className="navigation__item">
        <Link to="/docs" className="navigation__link">Docs</Link>
      </li>
      <li className="navigation__item">
        <Link to="/pricing" className="navigation__link">Pricing</Link>
      </li>
      <li className="navigation__item">
        <Link to="/about" className="navigation__link">About</Link>
      </li>
      <li className="navigation__item">
        <a href="http://blog.fuzzy.ai/" className="navigation__link">Blog</a>
      </li>
    </ul>
