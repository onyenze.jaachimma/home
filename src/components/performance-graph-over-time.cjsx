React = require 'react'
{ Line } = require 'react-chartjs-2' unless not window?
_ = require 'lodash'

{ Link } = require 'react-router'
{ connect } = require 'react-redux'

PerformanceGraph = React.createClass
  displayName: 'Performance Graph'

  render: ->
    { feedback, metric, width, height } = @props
    if not feedback or not window?
      return null

    width or= "100%"

    data = _.map feedback, (value, i) ->
      {x: i, y: value}

    data =
      datasets: [
        {
          type: "line",
          label: "Performance for #{metric}",
          fillColor: true,
          data: data,
          borderColor: '#001F27',
          backgroundColor: 'rgba(26,170,194,.5)',
          pointBorderColor: '#FAFBF3',
          pointBackgroundColor: '#001F27',
          hoverBackgroundColor: 'rgba(73, 48, 91, 0.75)',
          hoverBorderColor: 'rgba(73, 48, 91, 0.5))',
          pointBorderWidth: 2,
          pointHoverRadius: 1,
          pointHoverBackgroundColor: 'rgba(75,192,192,1)',
          pointHoverBorderColor: 'rgba(220,220,220,1)',
          pointHoverBorderWidth: 1,
          borderWidth: 1.5,
          pointRadius: 6,
          borderJoinStyle: 'miter'

        }
      ]

    chartOptions =
      datasetFill: false
      responsive: true
      maintainAspectRatio: true
      animation: false
      showTooltips: true
      tooltipFillColor: "#1f1f28"
      tooltipTitleFontColor: "#F8F6D1"
      customTooltips: true
      tooltipFontSize: 12
      tooltipYPadding: 18
      tooltipXPadding: 18
      tooltipCornerRadius: 2
      tooltipXOffset: 0
      legend: {
        display: true,
        position: 'top',
        fullWidth: true,
        fillStyle: '#8987aa'
        labels: {
          fontColor: '#3f3244'
          boxWidth: 12
          fontSize: 13

        }
      }
      scales:
        xAxes: [{
          type: 'linear',
          position: 'bottom'
        }]

    <Line data={data} options={chartOptions} id="performance-chart" />

mapStateToProps = (state) ->
  year: state.stats.year
  month: state.stats.month
  total: state.stats.total
  byDay: state.stats.byDay

module.exports = connect(mapStateToProps)(PerformanceGraph)
