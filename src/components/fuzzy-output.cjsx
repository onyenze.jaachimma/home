# fuzzy-sets.cjsx
# Copyright 2016 Fuzzy.ai <legal@fuzzy.ai>
# All rights reserved.

React = require 'react'
_ = require 'lodash'

{ Link } = require 'react-router'
{ findDOMNode } = require 'react-dom'
pcentroid = require '../centroid'

# Much thanks to Sebastian Markbåge (sebmarkbage) whose great gist on
# how to do canvas drawing with ReactJS helped make this possible.

FuzzySets = React.createClass

  displayName: 'Fuzzy sets'

  componentDidMount: ->

    canvas = findDOMNode this

    # XXX: some kind of hack. Necessary? Probably not.

    # canvas.width = canvas.width

    context = canvas.getContext '2d'
    @paint context

  componentDidUpdate: ->

    {width, height} = @props

    canvas = findDOMNode this

    # XXX: some kind of hack. Necessary? Probably not.

    # canvas.width = canvas.width

    context = canvas.getContext '2d'

    context.clearRect 0, 0, width, height

    @paint context

  render: ->

    {width, height} = @props

    <canvas width={width} height={height} />

  paint: (context) ->

    {width, height, padding, clipped, combined, centroid} = @props

    if _.isString width
      width = parseInt width, 10
    if _.isString height
      height = parseInt height, 10
    if _.isString padding
      padding = parseInt padding, 10

    # required properties
    if _.isEmpty clipped or _.isEmpty combined or _.isEmpty centroid
      return null

    minx = Infinity
    maxx = Number.NEGATIVE_INFINITY
    miny = 0
    maxy = 1.0

    for name, points of clipped
      for point in points
        [x, y] = point
        if x < minx
          minx = x
        if x > maxx
          maxx = x
        if y < miny
          miny = y
        if y > maxy
          maxy = y

    context.save()

    # Horizontal grid

    x = 0.5

    while x < width
      context.moveTo x, 0
      context.lineTo x, height
      x += 10

    # Vertical grid

    y = 0.5

    while y < height

      context.moveTo 0, y
      context.lineTo width, y
      y += 10

    context.strokeStyle = '#F6F6F3'
    context.stroke()

    # Top and bottom lines

    context.beginPath()
    context.moveTo 0, padding
    context.lineTo width, padding
    context.moveTo 0, height - padding
    context.lineTo width, height - padding
    context.strokeStyle = '#57546c'
    context.stroke()

    # Utilities for scaling

    sX = (x) ->
      padding + ((x - minx) / (maxx - minx)) * (width - 2 * padding)

    sY = (y) ->
      padding + ((maxy - y) / (maxy - miny)) * (height - 2 * padding)

    # Scale lines

    raw = (maxx - minx) / 20.0
    smooth = undefined

    # XXX: handle < 1

    n = 1
    while n < raw
      n *= 10
    if n / raw > 1
      n /= 2
    j = 0
    while j <= maxx - minx
      context.beginPath()
      context.moveTo sX(j), sY(0.05)
      context.lineTo sX(j), sY(-0.05)
      context.strokeStyle = '#45384b'
      context.stroke()
      context.textAlign = 'center'
      context.textBaseline = 'top'
      context.fillText j.toString(), sX(j), sY(-0.06)
      j += n
    i = 0
    colours = [
      [105, 78, 124]
      [83, 180, 196]
      [234, 70, 85]
      [69, 56, 75]
      [249, 86, 79]
      [105, 78, 124]
      [83, 180, 196]
      [225,47,62]
      [255,250,227]
      [231,76,60]
      [237.38,71]
    ]

    for setName, points of clipped

      context.beginPath()
      [r, g, b] = colours[i % colours.length]
      a = 0.75
      context.fillStyle = "rgba(#{r},#{g},#{b},#{a})"
      context.moveTo sX(points[0][0]), sY(points[0][1])
      for point in points.slice(1)
        context.lineTo sX(point[0]), sY(point[1])
      context.strokeStyle = '#45384b'
      context.stroke()
      context.fill()
      # Draw the name at the center... kind of
      context.fillStyle = '#57546c'
      context.font = '12px sans-serif'
      context.textAlign = 'center'
      context.textBaseline = 'middle'
      scpt = pcentroid points
      context.fillText setName, sX(scpt[0]), sY(scpt[1])
      i++

    # Line showing the combined polygon

    context.beginPath()
    context.lineWidth = 1.0
    [r, g, b] = colours[i % colours.length]
    context.fillStyle = "rgba(59,66,75,0.1)"
    context.moveTo sX(combined[0][0]), sY(combined[0][1])
    for point in combined.slice(1)
      context.lineTo sX(point[0]), sY(point[1])
    context.lineTo sX(combined[0][0]), sY(combined[0][1])
    context.strokeStyle = '#57546c'
    context.stroke()
    context.fill()

    # Target for centroid

    context.beginPath()
    context.lineWidth = 1.0
    context.strokeStyle = '#57546c'
    context.arc sX(centroid[0]), sY(centroid[1]), 5, 0, 2 * Math.PI
    context.arc sX(centroid[0]), sY(centroid[1]), 10, 0, 2 * Math.PI
    context.stroke()

    # Line showing the centroid

    context.beginPath()
    oldDash = context.getLineDash()
    context.setLineDash([3, 3])
    context.lineWidth = 1.0
    context.moveTo sX(centroid[0]), sY(miny)
    context.lineTo sX(centroid[0]), sY(maxy)
    context.moveTo sX(minx), sY(centroid[1])
    context.lineTo sX(maxx), sY(centroid[1])
    context.strokeStyle = '#57546c'
    context.stroke()
    context.setLineDash oldDash

    context.restore()

module.exports = FuzzySets
