React = require 'react'
{ Link } = require 'react-router'

module.exports = React.createClass
  displayName: "Docs"

  # coffeelint: disable=max_line_length
  render: ->
    <div>
      <h1>RESTful API</h1>

      <p>Fuzzy.ai provides a RESTful API so that applications can make evaluations remotely.</p>

      <p>If possible, consider one of the <Link to="/docs/sdk">SDK</Link> libraries provided for your preferred programming language. They are much easier to program for than a REST API.</p>
    </div>
