React = require 'react'
{ Link } = require 'react-router'

CrossPlatformIcon = require './cross-platform-svg-icon'
PricingIcon = require './pricing-svg-icon'
VersatileIcon = require './versatile-svg-icon'

HomeFuzzyValues = require './home-fuzzy-values'

module.exports = React.createClass

  displayName: "home grid"

  # coffeelint: disable=max_line_length
  render: ->
    <div className="page">
      <main className="grid-wrap fuzzy-home-values">
        <div className="flex-box-grid fuzzy-home-values__col">
          <div className="fuzzy-home-values__content txtC">
            <div className="cross-platform-icon svg-icon-wrap">
              <CrossPlatformIcon />
            </div>
            <HomeFuzzyValues
              name= "Cross-Platform"
              description= "SDKs and a RESTful API make it easy to add intelligent decision-making capabilities to web, desktop and mobile software."
              btnLink= "/docs"
              btnName="View Documentation"
            />
          </div>
        </div>
        <div className="flex-box-grid fuzzy-home-values__col " >
          <div className="fuzzy-home-values__content txtC">
            <div className="pricing-icon svg-icon-wrap ">
              <PricingIcon />
            </div>
            <HomeFuzzyValues
              name="Simple Pricing"
              description="Make up to 5,000 API calls per month for free. No credit card required to sign up."
              btnLink="/pricing"
              btnName="View Pricing"
            />
          </div>
        </div>
        <div className=" flex-box-grid fuzzy-home-values__col">
          <div className="fuzzy-home-values__content txtC">
            <div className="versatile-icon svg-icon-wrap">
              <VersatileIcon />
            </div>
            <HomeFuzzyValues
              name="Versatile"
              description="Use Fuzzy.ai for pricing decisions, lead scoring, social
              content ranking, social network analysis, text analysis,
              and fraud detection."
              btnLink="/how-it-works"
              btnName="More info"
            />
          </div>
        </div>
      </main>
    </div>
