React = require 'react'
{ Link } = require 'react-router'
{ Facebook, Twitter, LinkedIn, AngelCo } = require './icons'

module.exports = React.createClass

  displayName: "fuzzy social footer"

  componentDidMount: ->
    backtoTop = document.querySelector('.backtotop')
    app = document.querySelector('#app')

    scrollUp = (e) ->
      e.preventDefault()
      app.scrollIntoView
        behavior: 'smooth'
      return

    backtoTop?.addEventListener 'click', scrollUp


  render:->

    <div className="footer_end">
      <div className="social-links-wrapper">
        <ul className="social-links">
          <li className="social-links__items">
            <div className="icon icon-twitter">
            <Twitter
              link='https://www.twitter.com/fuzzyai'
              share='twitter'
              shareUrl='https://twitter.com/share?url='
              width= 24
              height= 24
            />
            </div>
          </li>
          <li className="social-links__items">
            <div className="icon icon-facebook">
            <Facebook
              link='https://www.facebook.com/fuzzy.io'
              share='facebook'
              shareUrl='https://www.facebook.com/fuzzy.io'
              width= 24
              height= 24
            />
            </div>
          </li>
          <li className="social-links__items">
            <div className="icon icon-angel">
            <AngelCo
              link='https://angel.co/fuzzy-ai'
              share='Angel Co'
              shareUrl='https://angel.co/fuzzy-ai'
              width= 24
              height= 24
            />
            </div>
          </li>
          <li className="social-links__items">
          <div className="icon icon-linkedin">
          <LinkedIn
            link='https://www.linkedin.com/company/fuzzy-io'
            share='LinkedIn'
            shareUrl='https://www.linkedin.com/company/fuzzy-io'
            width= 24
            height= 24
          />
          </div>
          </li>
        </ul>
      </div>
      <span className="backtotop"><a href="#" onClick={@scrollUp}></a></span>
    </div>
