_ = require 'lodash'
React = require 'react'

Dropdown = require './dropdown'
Autocomplete = require './autocomplete'
RangeInput = require './range-input-popup'

IsExpression = React.createClass

  getInitialState: ->
    type: 'is'
    dimension: @props.json?.dimension
    set: @props.json?.set
    showInputSettings: false
    inputSets: null
    rangeOverlay: false

  componentDidUpdate: (prevProps, prevState) ->
    { handleChange, agent } = @props
    if not _.isEqual prevState, @state
      json =
        type: 'is'
        dimension: @state.dimension
        set: @state.set

      handleChange json, agent

  handleDimensionChange: (value) ->
    { agent } = @props
    @setState dimension: value
    if not agent.inputs[value]?[@state.set]
      @setState set: ""

  handleDimensionBlur: (value, options) ->
    if not options?.length
      @handleInputSets([0, 100])
      @setState showInputSettings: true

  handleSetChange: (value) ->
    @setState set: value

  toggleRange: (e) ->
    e.preventDefault()
    @setState showInputSettings: true
    @setState rangeOverlay: not @state.rangeOverlay


  handleInputSets: (sets) ->
    @setState
      inputSets: sets
      showInputSettings: false
    # need to save the input
    { agent, agentHandler, handleChange } = @props
    agent.inputs[@state.dimension] = sets
    agentHandler(agent)

  handleRangeClose: ->
    if not @state.inputSets
      @handleInputSets([0, 100])
    @setState showInputSettings: false


  handleChangeType: (value) ->
    { handleChange, agent } = @props

    left =
      type: 'is'
      dimension: @state.dimension
      set: @state.set

    right =
      type: 'is'
      dimension: ''
      set: ''

    expression =
      type: value
      left: left
      right: right

    handleChange expression, agent

  formatSet: (set) ->
    if _.isArray(set)
      "(#{_.min(set)} - #{_.max(set)})"

  # coffeelint: disable=max_line_length
  render: ->
    {rangeOverlay, showInputSettings } = @state
    { json, agent, editMode } = @props
    options = [
      {value: "and", label: 'and'},
      {value: "or", label: 'or'}
    ]

    dimension = @state.dimension
    formatSet = @formatSet
    setOptions = _.map _.keys(agent.inputs[dimension]), (set) ->
      {value: set, label: "#{set} #{formatSet(agent.inputs[dimension]?[set])}"}

    setValue = @state.set
    if agent.inputs[dimension]?[@state.set]
      setValue = "#{@state.set} #{@formatSet(agent.inputs[@state.dimension]?[@state.set])}"
    <div className="inline">
    <span>
      <span className="rule-form__var-antecedent-dimension user-input-text" >
      {if editMode
        <span className="rule-form__agent-input">
          <Autocomplete value={@state.dimension} items={_.keys(agent.inputs)} onChange={@handleDimensionChange} placeholder="name of input" onBlur={@handleDimensionBlur} />
        </span>
      else
        json?.dimension
      }
      </span>
      <span className="rule-form__var-antecedent-type user-input-text"> {json?.type} </span>
      <span className="rule-form__var-antecedent-set user-input-text">
      {if editMode
        <Dropdown title={setValue || "value of input"} options={setOptions} selectHandler={@handleSetChange} />
      else
        setValue
      }
      </span>
      {if editMode
        <span className="rule-form__change-type">
          <Dropdown title=" + " options={options} selectHandler={@handleChangeType} />
        </span>
      }
    </span>
    {if editMode
      <RangeInput title={@state.dimension} input={agent.inputs[@state.dimension]} saveHandler={@handleInputSets} placeholder="Value of input" />
    }
    </div>


module.exports = IsExpression
