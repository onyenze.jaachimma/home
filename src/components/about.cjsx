React = require 'react'
{ Link } = require 'react-router'

TeamMember = require './team-member'
PageTitle = require './page-title-header-section'


module.exports = React.createClass
  displayName: "About"

  # coffeelint: disable=max_line_length
  render: ->
    <div className="row site-wrap">
      <PageTitle
        title ="About us"
        subtitle ={['Fuzzy.ai is built by a team of seasoned entrepreneurs with the mission of making ' , <span  key="ai" className="accent inline">AI</span>, ' and machine learning accessible to all developers.']}
        desc1 ='Our team of five has over 90 years of combined experience working on products like Gmail, Google Calendar, Facebook, Wikitravel, Drupal, Status.Net and Agendize.'
        desc2 ='Our investors include 500 Startups, Real Ventures, Interaction Ventures and iNovia Capital.'
        desc3 ={[<Link to="/how-it-works"  key="learnMore" className="fwb"> Want to learn more?</Link>]}
      />

      <div className="row-inner">
        <div className="page">
         <div className="team_listing">
          <h2 className="fwn">Meet the team</h2>
          <TeamMember name="Evan Prodromou"
                      image="../images/team-fuzzy-evan.jpg"
                      title="Founder"
                      twitter="evanpro"
                      linkedin="evanprodromou"
                      angellist="evan-prodromou" />
          <TeamMember name="Matt Fogel"
                      image="../images/team-fuzzy-matt.jpg"
                      title="Founder"
                      twitter="mattfogel"
                      linkedin="mattfogel"
                      angellist="mattfogel" />
          <TeamMember name="Kevin Fox"
                      image="../images/team-fuzzy-kevin.jpg"
                      title="Chief Experience Officer"
                      twitter="kfury"
                      linkedin="person"
                      angellist="kfury" />
          <TeamMember name="James Walker"
                      image="/assets/i/james-walker.jpg"
                      title="Lead Software Developer"
                      twitter="walkah"
                      linkedin="walkah"
                      angellist="walkah" />
          <TeamMember name="Pablo Boerr"
                      image="/assets/i/pablo-boerr.jpg"
                      title="Lead Product Designer"
                      twitter="pab_mtl"
                      linkedin="pabloboerr"
                      angellist="pablo-boerr" />
          </div>
        </div>
      </div>
    </div>
