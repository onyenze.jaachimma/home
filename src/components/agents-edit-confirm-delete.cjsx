React = require 'react'
Link = require 'react-router/lib/Link'
{ connect } = require 'react-redux'
{ deleteAgent} = require '../actions/agent'

AgentsDelete = React.createClass
  displayName: 'Confirm Delete'

  getInitialState: ->
    error: ''

  componentDidMount: ->
    @refs.agentDelete.focus()
    return

  handleSubmit: (e) ->
    inputValue= document.querySelector('.input-delete').value
    { agent, dispatch } = @props
    e.preventDefault()
    if inputValue == agent.name
      dispatch(deleteAgent(agent))
    else
      @setState error: 'To permanently delete this Agent, please make sure the name of your agent is right.'


  # coffeelint: disable=max_line_length
  render: ->
    { agent, handleDelete } = @props
    {error} = @state

    <div className="agent__element">
      {if error
        <p className="error_message">{ error }</p>
      }
      <p className="message warning big-warning"><em>This action cannot be undone. This will permanently delete </em><b>{agent?.name}</b>?</p>
      <form onSubmit={@handleSubmit} className="pad-40">
      <p>Please enter the name of the agent below to confirm you want to permanently delete it</p>
      <input type="text" ref="agentDelete" className="input-delete" placeholder="Enter name of agent to delete"/>
        <button className="btn btn__regular" type="submit">Delete</button>
        <Link to="/agents/#{agent?.id}" className="btn__regular btn--cancel">Cancel</Link>
      </form>
    </div>

mapStateToProps = (state) ->
  agent: state.agent.current
  error: state.agent.error

module.exports = connect(mapStateToProps)(AgentsDelete)
