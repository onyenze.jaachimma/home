_ = require 'lodash'
React = require 'react'
Autocomplete = require './autocomplete'
Dropdown = require './dropdown'
FuzzyExpression = require './fuzzy-expression'
RangeInput = require './range-input-popup'

AgentsEditRuleIfThen = React.createClass
  displayName: "AgentsEditRuleIfThen"

  getInitialState: ->
    editMode: false
    antecedent: @props.rule?.antecedent || {type: 'is', dimension: '', set: ''}
    consequent: @props.rule?.consequent || {type: 'is', dimension: '', set: ''}

  componentDidMount: ->
    { rule, isNew, agent } = @props
    if isNew
      @setState editMode: true

    if not @state.consequent.dimension
      dimension = _.keys(agent.outputs)[0]
      consequent = _.assign {}, @state.consequent, {dimension: dimension}
      @setState consequent: consequent

  toggleEdit: (e) ->
    e.preventDefault()
    @setState editMode: not @state.editMode
    @props.toggleGlobalEdit()

  handleSubmit: (e) ->
    e.preventDefault()

    { agent, rule, id, submitHandler } = @props

    rule.type = "if-then"
    rule.antecedent = @state.antecedent
    rule.consequent = @state.consequent

    weight = @refs.weight?.value * 1
    if _.isNumber(weight) and weight >= 0.0 and weight <= 1.0
      rule.weight = weight

    submitHandler(agent, rule, id)
    @setState editMode: false
    @props.toggleGlobalEdit?()

  handleDelete: (e) ->
    e.preventDefault()

    { agent, id, deleteHandler } = @props
    if window.confirm("Are you sure?")
      deleteHandler(agent, id)

  handleConsequentSetChange: (value) ->
    parts = _.split(value, '(', 1)
    consequent = _.assign {}, @state.consequent, {set: _.trim(parts[0])}
    @setState consequent: consequent

  handleAntecedentChange: (expression, agent) ->
    @setState antecedent: expression

  formatSet: (set) ->
    if _.isArray(set)
      "(#{_.min(set)} - #{_.max(set)})"

  # coffeelint: disable=max_line_length
  render: ->
    { editMode, showInputRange } = @state
    { rule, agent, isNew, agentHandler, inProgress } = @props

    if editMode
      ruleClass = "rule edit-mode"
      handleToggle = ->
    else
      handleToggle = @toggleEdit
      if @props.globalEdit
        ruleClass = "rule no-edit"
      else
        ruleClass = "rule"

    dimension = @state.consequent.dimension
    formatSet = @formatSet
    setOptions = _.map _.keys(agent.outputs[dimension]), (set) ->
      {value: set, label: "#{set} #{formatSet(agent.outputs[dimension]?[set])}"}

    setValue = @state.consequent.set
    if agent.outputs[dimension]?[@state.consequent.set]
      setValue = "#{@state.consequent.set} #{@formatSet(agent.outputs[dimension]?[@state.consequent.set])}"

    <div className={ruleClass} onClick={handleToggle}>
      <svg viewBox="0 0 32 32" className="edit-hint ui-icon">
        <use xlinkHref="/assets/sprite.svg#icon-pencil"></use>
      </svg>
      <form onSubmit={@handleSubmit} className="rule-form">
        <div className="rule-form__text-wrap">
          <div className="rule-form__text-wrap-inner">
            <span className="rule-form__function">If </span>

            <FuzzyExpression json={@state.antecedent} agent={agent} handleChange={@handleAntecedentChange} editMode={editMode} agentHandler={agentHandler} />

            <span className="rule-form__function"> then </span>

            <span className="rule-form__var-consequent-dimension">{ @state.consequent?.dimension}</span>
            <span className="rule-form__var-consequent-type"> { @state.consequent?.type} </span>
            <span className="rule-form__var-consequent-set">
              {if editMode
                <Dropdown title={setValue || "value of output"} options={setOptions} selectHandler={@handleConsequentSetChange} />
              else
                setValue
              }
            </span>

            {if editMode
              <span className="rule-weight">
                <label htmlFor="weight">Set the Weight</label>
                <input type="number" min="0.0" max="1.0" step="0.01" className="rule-form__input" ref="weight" defaultValue={rule?.weight} />
              </span>
            else if rule?.weight
              <span className="rule-form__display-weight"> Weight: {rule?.weight}</span>
            }
          </div>
        </div>
          {if not isNew
            <button className="rule-form-actions__cancel rule-btn" onClick={@toggleEdit}>
              <svg viewBox="0 0 32 32" className="cancel ui-icon">
                <use xlinkHref="/assets/sprite.svg#icon-cross"></use>
              </svg>
            </button>
          }
          {if editMode
            <div className="rule-form-actions ">
              { if inProgress
                <button type="submit" className="rule-form-actions__save rule-btn" disabled="disabled">Working...</button>
              else
                <button type="submit" className="rule-form-actions__save rule-btn">Save</button>
              }
              <button className="rule-form-actions__delete rule-btn" onClick={@handleDelete}>
                <svg className="delete-icon" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 32 32">
                  <use xlinkHref="/assets/sprite.svg#icon-trash"></use>
                </svg>
              </button>
            </div>
          else
            <div className="rule-form-actions action-hover">
              <button onClick={@toggleEdit} className="rule-form-actions__edit rule-btn ir">
                <svg className="pen" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 61.97 61.96">
                  <g className="pencil">
                    <path className="tip" className="cls-1" d="M0 61.96l16.16-5.39L5.39 45.8 0 61.96z"/>
                    <path className="eraser" className="cls-1" d="M41.76 9.43L51.184.004 61.96 10.78l-9.425 9.426z"/>
                    <path className="pen--body" className="cls-1" d="M7.304 44.36l32.23-32.23L49.85 22.444l-32.23 32.23z"/>
                  </g>
                </svg>
              </button>
            </div>
          }
      </form>
    </div>

module.exports = AgentsEditRuleIfThen
