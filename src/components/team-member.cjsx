React = require 'react'
{ Link } = require 'react-router'
{ AboutTwitter, AboutLinkedIn, AboutAngelCo } = require './icons'
module.exports = React.createClass

  displayName: "Team member"

  # coffeelint: disable=max_line_length
  render: ->
    <div className="team-member">
      <figure>
      <img src={@props.image} alt={@props.name + " " + @props.title} className="team" />
      </figure>
        <div className="team-member__text">
          <h3 className="team-member__name mbs fwn">{@props.name}</h3>
          <span className="team-member__position">{@props.title}</span>
            <div className="team-member__social social-links-wrapper">
              <ul className="team-member__social-list social-links">
                <li className="team-member__social-list__item social-links__item">
                  <a href={"https://twitter.com/" + @props.twitter} className="social-links__items-anchor">
                    <AboutTwitter />
                  </a>
                </li>
                <li className="team-member__social-list__item social-links__item">
                  <a href={"https://www.linkedin.com/in/" + @props.linkedin} className="social-links__items-anchor">
                    <AboutLinkedIn />
                  </a>
                </li>
                <li className="team-member__social-list__item social-links__item">
                  <a href={"https://angel.co/" + @props.angellist} className="social-links__items-anchor">
                    <AboutAngelCo />
                  </a>
                </li>
              </ul>
            </div>
          </div>
        </div>
