_ = require 'lodash'
React = require 'react'
CSSTransitionGroup = require 'react-addons-css-transition-group'
Link = require 'react-router/lib/Link'
Plan = require './plan'
{ connect } = require 'react-redux'
{ fetchAgent } = require '../actions/agent'
{ editRule, deleteRule } = require '../actions/agent'

AgentsNav = require './agents-nav'

{ GearIcon, EditIcon, BeakerIcon, StatsIcon } = require './icons'

EditRules = require './agents-edit-rules'
Clipboard = require './clipboard'
ConfirmDelete = require './agents-edit-confirm-delete'
Settings = require './agents-edit-settings'
SetOutput = require './agents-edit-set-output'
ProgressBar = require './progress-bar'
ErrorBar = require './error-bar'

AgentsEdit = React.createClass
  displayName: 'AgentsEdit'

  componentDidMount: ->
    { dispatch, params, agent } = @props
    dispatch(fetchAgent(params.agentID))


  # coffeelint: disable=max_line_length
  render: ->
    { agent, error, inProgress} = @props
    { location } = @props.location.pathname



    <div className="agent-edit">
      {if error?
        <ErrorBar err={error} />
      }
      <div className="agent">

        <div className="agent__heading">
          <AgentsNav agent={agent}  location={@props.location} />
          <CSSTransitionGroup
            component="div"
            className="agent__heading-wrapper"
            transitionAppear={true}
            transitionAppearTimeout={900}
            transitionEnter={false}
            transitionLeave={false}
            transitionName="agent__heading-wrapper"
            key="agent-heading"
            >
            <h1 className="agent__name" >
              <span className="agent__id line"><Clipboard copyText={ agent?.id } /></span>
              <span className="app-heading">{ agent?.name }</span>
            </h1>
            <div className="agent__description">
              <p>{ agent?.description }</p>
            </div>
          </CSSTransitionGroup>
        </div>
        <CSSTransitionGroup
        transitionName="agent-content"
        className="agent-content"
        component="div"
        transitionAppear={true}
        transitionAppearTimeout={1500}
        transitionEnter={true}
        transitionEnterTimeout={300}
        transitionLeave={true}
        transitionLeaveTimeout={400}
        key="agent-content">
        { @props.children && React.cloneElement(@props.children, {key: @props.location.pathname }, {agent: agent})}
        </CSSTransitionGroup>
      </div>
    </div>

mapStateToProps = (state) ->
  agent: state.agent.current
  error: state.agent.error
  inProgress: state.agent.inProgress

module.exports = connect(mapStateToProps)(AgentsEdit)
