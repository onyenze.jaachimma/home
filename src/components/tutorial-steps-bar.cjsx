React = require 'react'
{ DeleteCross } = require './icons'
{ Link } = require 'react-router'

TutorialStepBar = React.createClass
  displayName: "TutorialStepBar"

  getInitialState: ->
    stepBarActive: not @props.completed

  toggleStepBar: ->
    @setState stepBarActive: false

  # coffeelint: disable=max_line_length
  render: ->
    {step, completed} = @props

    {stepBarActive} = @state

    if stepBarActive
      stepBarClass = "tut-bar-wrapper"
      handleStepBarToggle  = @toggleStepBar
    else
      stepBarClass = "tut-bar-wrapper tut-bar-wrapper-is-closed"
      handleStepBarToggle  = @toggleStepBar

    <div className={stepBarClass}>
    {if step?
      <div className="continue-tuts-bar__outer">
        <Link to="/tutorial/#{step}" className="continue-tuts-bar ">
        <span className="upper spaced-normal fwb continue-tuts-bar__text">Need help? <span className="accent">check out the tutorial!</span></span>
        </Link>
        <span className="close-tut-bar" onClick={handleStepBarToggle}>
        <DeleteCross />
        </span>
      </div>
    else
      <div className="continue-tuts-bar__outer">
        <Link to="/tutorial" className="continue-tuts-bar ">
        <span className="upper spaced-normal fwb continue-tuts-bar__text">Need help? <span className="accent">check out the tutorial!</span></span>
        </Link>
        <span className="close-tut-bar" onClick={handleStepBarToggle}>
        <DeleteCross />
        </span>
      </div>
    }
    </div>

module.exports = TutorialStepBar
