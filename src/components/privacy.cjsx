React = require 'react'
{ Link } = require 'react-router'

module.exports = React.createClass
  displayName: "Privacy"

  # coffeelint: disable=max_line_length
  render: ->
    <div className="row">
      <div className="main-80 privacy-text small-text">
        <h2 className="mbt fwb">Privacy Policy</h2>
        <p>9165584 Canada Corporation D/B/A Fuzzy.ai (&ldquo;Fuzzy.ai&rdquo;) provides a platform and API for developers to facilitate the use of artificial intelligence and machine learning technologies in their own software. Fuzzy.ai is committed to protecting the privacy of its users&rsquo; data.</p>

        <h3 className="mbt fwb">Purpose and Scope of the Policy</h3>

        <p>This privacy policy describes how we collect, use and disclose your personal information and how this personal information can be consulted and corrected when necessary. This policy also explains how cookies are used on our Website. </p>

        <p>By visiting http://fuzzy.ai (the &ldquo;Website&rdquo;), contacting us or agreeing to receive emails from Fuzzy.ai, you accept the terms and conditions of this policy.</p>

        <p>This policy does not extend to websites operated by third parties. Fuzzy.ai is therefore not liable for their privacy policies, procedures and practices. </p>

        <h3 className="mbt fwb">Personal Information Collected </h3>
        <p><strong>A.	Information that you send us </strong></p>

        <p>We collect your personal information in several ways, including when you create an account on our service, provide billing information for a paid subscription, sign up for our newsletter or contact us via telephone, email or live chat. </p>

        <p>Depending on the circumstances, personal information may include your name, contact information (street address, electronic address, telephone number), method of payment, and financial information.  </p>

          <p><strong>B.	Information provided on our Website </strong></p>

          <p>We also collect data relating to your visit to our Website. This information may include your IP address, the date, time and duration of your visits as well as the web pages you consulted. </p>

          <p>Our Website, like most other commercial websites, also uses cookies. These are files that are installed on your computer hard drive or web browser in order to collect information such as your language of preference, browsing history and browser type and version, all for the purpose of optimizing your experience on our Website. </p>

          <p>These cookies cannot be used to extract personal information. However, we can match the information collected by cookies with other information through the Website [OR] the information collected by cookies is not related to any other data that can identify you. </p>

          <p>Disabling cookies on your browser could adversely affect your browsing experience on the Website.</p>

          <h3 className="mbt fwb">Use and Communication of your Personal Information</h3>
          <p>We use and disclose your personal information mainly: </p>

          <ul className="task-list bullet-list compressed-list">
            <li className="bullet-list__item compressed-list__item">To allow us to provide our products and services; </li>
            <li className="bullet-list__item compressed-list__item">To answer questions and information requests; </li>
            <li className="bullet-list__item compressed-list__item">For the purposes of marketing, advertising or contests; </li>
            <li className="bullet-list__item compressed-list__item">For any other purposes authorized or required by law. </li>
          </ul>
          <p>When we disclose your personal information to third parties, we take reasonable measures to ensure that the rules set forth in this policy are complied with.</p>

          <h3 className="mbt fwb">Right to Access and Correct</h3>
          <p>On written request and subject to proof of identity, you may consult the personal information that we have disclosed, and ask that any necessary corrections be made, where applicable, as authorized or required by law.</p>

          <p>However, to make sure that the personal information we maintain about you is accurate and up to date, please inform us immediately of any change in your personal information.  </p>
          <h3 className="mbt fwb">Update</h3>
          <p>This Privacy Policy may be amended without notice. This policy was last updated on June 23, 2015. </p>

          <h3 className="mbt fwb">Contact</h3>
          <p>Any request to access or correct your personal information and any question or comment you may have with respect to our privacy policy must be sent to the Privacy Officer by mail or email using the following contact information:
          </p>

          <p>9165584 Canada Corporation D/B/A Fuzzy.ai
          <br />
          5333 AVE CASGRAIN SUITE 1227
          <br />
          MONTREAL, QC H2T 1X3
          <br />
          legal@fuzzy.ai
          </p>

      </div>
    </div>
