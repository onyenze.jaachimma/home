_ = require 'lodash'
React = require 'react'
{ Link } = require 'react-router'
{ connect } = require 'react-redux'

APIUsageChart = require './api-usage-chart'
Clipboard = require './clipboard'


{ fetchAgents } = require '../actions/agent'
{ fetchStats } = require '../actions/stats'



class DashboardApiKey extends React.Component
  displayName: 'Dashboard Api Key'

  componentDidMount: ->
    { dispatch, user } = @props

    agent_list = {}


  # coffeelint: disable=max_line_length
  render: ->
    {agents, user} = @props

    <div className="dashboard-api dash-modules">
      <div className="dashboard-api__details">
        <span className="sup-heading line upper fwb mbst">Your API key:</span>
        <span className="dash-modules__data"><Clipboard copyText={@props.user?.defaultToken} /></span>
      </div>
    </div>



mapStateToProps = (state) ->
  agents: state.agent.items
  user: state.user.account
  authenticated: state.user.authenticated

module.exports = connect(mapStateToProps)(DashboardApiKey)
