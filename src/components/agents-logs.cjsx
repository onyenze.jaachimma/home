React = require 'react'
{ Link } = require 'react-router'
{ connect } = require 'react-redux'

LogList = require './log-list'
ErrorBar = require './error-bar'

{ fetchAgent } = require '../actions/agent'
{ fetchAgentLogs } = require '../actions/logs'

OFFSET = 0
LIMIT = 20

AgentsLogs = React.createClass

  displayName: 'Agents logs'

  componentDidMount: ->
    {dispatch, params} = @props
    dispatch(fetchAgent(params.agentID))
    page = parseInt(params.page, 10)
    offset = (params.page - 1) * LIMIT
    # We ask for one more to see if we need a paging link
    limit = LIMIT + 1
    dispatch(fetchAgentLogs(offset, limit, params.agentID))

  # coffeelint: disable=max_line_length
  render: ->
    {agent, logs, params, agentError, logsError} = @props
    page = parseInt(params.page, 10)

    logs = logs or []

    <div className="row app-row-bg">
      <div className="page">
        {if agentError?
          <ErrorBar err={agentError} />
        }
        {if logsError?
          <ErrorBar err={logsError} />
        }
        <LogList logs={logs.slice(0, LIMIT)} agents={[agent]} />
        { if logs.length > LIMIT and agent?
          <p>
            <Link to="/agents/#{agent.id}/logs/#{page+1}">More...</Link>
          </p>
        }
      </div>
    </div>

mapStateToProps = (state) ->
  agent: state.agent.current
  agentError: state.agent.error
  logs: state.logs.logs
  logsError: state.logs.error

module.exports = connect(mapStateToProps)(AgentsLogs)
