_ = require 'lodash'
React = require 'react'
{ Link } = require 'react-router'
{ connect } = require 'react-redux'

APIUsageChart = require './api-usage-chart'


{ fetchAgents } = require '../actions/agent'
{ fetchPlan } = require '../actions/plans'
{ fetchStats } = require '../actions/stats'

OFFSET = 0
LIMIT = 5

class DashboardApiStats extends React.Component
  displayName: 'Dashboard Meta'

  componentDidMount: ->
    { dispatch, user } = @props




  # coffeelint: disable=max_line_length
  render: ->
    {agents, logs, plan, total, user} = @props
    {agentInProgress, userInProgress, logsInProgress, statsInProgress, plansInProgress} = @props
    inProgress = agentInProgress || userInProgress || logsInProgress || statsInProgress || plansInProgress


    <div className="dash-modules">
      <span className="sup-heading line upper fwb mbst">Api calls</span>
      <span className="dash-modules__data upper">{total}/{plan?.apiLimit}</span>
    </div>




mapStateToProps = (state) ->
  agentInProgress: state.agent.inProgress
  userInProgress: state.user.inProgress
  statsInProgress: state.stats.inProgress
  plansInProgress: state.plans.inProgress
  agents: state.agent.items
  user: state.user.account
  authenticated: state.user.authenticated
  total: state.stats.total
  plan: state.plans.current



module.exports = connect(mapStateToProps)(DashboardApiStats)
