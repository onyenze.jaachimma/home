_ = require 'lodash'
React = require 'react'
Link = require 'react-router/lib/Link'
Plan = require './plan'
{ connect } = require 'react-redux'
{ fetchAgentStats } = require '../actions/stats'
{ fetchAgentLogs } = require '../actions/logs'

PerformanceGraph = require './performance-graph-over-time'
APIUsageChart = require './api-usage-chart'
ErrorBar = require './error-bar'

AgentsStats = React.createClass
  displayName: 'Agent Stats'

  componentDidMount: ->
    { dispatch, params } = @props
    now = new Date()
    year = now.getUTCFullYear()
    month = now.getUTCMonth() + 1
    dispatch(fetchAgentStats(year, month, params.agentID))
    dispatch(fetchAgentLogs(0, 100, params.agentID))

  previousMonth: =>

    {dispatch, year, month} = @props

    if month == 1
      previousYear = year - 1
      previousMonth = 12
    else
      previousYear = year
      previousMonth = month - 1

    dispatch(fetchStats(previousYear, previousMonth))

    false

  nextMonth: =>

    {dispatch, year, month} = @props

    if month == 12
      nextYear = year + 1
      nextMonth = 1
    else
      nextYear = year
      nextMonth = month + 1

    dispatch(fetchStats(nextYear, nextMonth))

    false

  # coffeelint: disable=max_line_length
  render: ->
    { agent, byDay, logs, year, month, agentError, statsError } = @props

    monthNames = [
      "January"
      "February"
      "March"
      "April"
      "May"
      "June"
      "July"
      "August"
      "September"
      "October"
      "November"
      "December"
    ]

    monthName = monthNames[month - 1]

    now = new Date()

    thisYear = now.getUTCFullYear()
    thisMonth = now.getUTCMonth() + 1

    created = new Date(agent?.createdAt)

    createdYear = created.getUTCFullYear()
    createdMonth = created.getUTCMonth() + 1

    feedbackForMetric = (evaluationLogs, metric) ->
      _.map evaluationLogs, (log) ->
        log.feedback?[metric]

    <div className="agent-stats-wrapper">

      {if agentError?
        <ErrorBar err={agentError} />
      }
      {if statsError?
        <ErrorBar err={statsError} />
      }
          <div className="arrow-nav-wrapper arrow-nav">
            <div className="arrow-nav-wrapper-inner">
              <span className="sup-heading line upper fwb mbst txtC">API Usage</span>
              <h3 className="dash-modules__data mbst txtC">for {monthName} {year}</h3>
              { if year > createdYear or (year == createdYear and month > createdMonth)

                <a href="#" onClick={@previousMonth} className="arrow-nav__prev-arrow prev-month arrow">
                  <span>
                    <svg viewBox="0 0 32 32" className="ui-icon left-arrow">
                      <use xlinkHref="/assets/sprite.svg#icon-chevron-left"></use>
                    </svg>
                  </span>
                </a>
                }
                { if year < thisYear or (year == thisYear and month < thisMonth)

                  <a href="#" onClick={@nextMonth} className="arrow-nav__next-arrow next-month arrow">
                    <span>
                      <svg viewBox="0 0 32 32" className="ui-icon left-arrow">
                        <use xlinkHref="/assets/sprite.svg#icon-chevron-right"></use>
                      </svg>
                    </span>
                  </a>
                }
              </div>
            </div>

      <div className="aspect-ratio">
        <APIUsageChart byDay={byDay} />
      </div>
      {_.map agent?.performance, (type, metric) ->
        feedback = feedbackForMetric(logs, metric)
        <div className="aspect-ratio">
          <span className="sup-heading line upper fwb mbst txtC">Performance for</span>
          <h3 className="app-heading h3 fwb mbst txtC">{metric}</h3>
          <br />
          <PerformanceGraph metric={metric} feedback={feedback} />
        </div>
      }
    </div>

mapStateToProps = (state) ->
  agent: state.agent.current
  agentError: state.agent.error
  statsError: state.stats.error
  year: state.stats.year
  month: state.stats.month
  logs: state.logs.logs

module.exports = connect(mapStateToProps)(AgentsStats)
