React = require 'react'
{ Link } = require 'react-router'
{ connect } = require 'react-redux'
{ push } = require 'react-router-redux'
_ = require 'lodash'

LogList = require './log-list'
ErrorBar = require './error-bar'

{ fetchAgents } = require '../actions/agent'
{ fetchLogs } = require '../actions/logs'

OFFSET = 0
LIMIT = 20
DASHBOARD_LIMIT = 5

MoreLogs = React.createClass

  displayName: 'More logs'

  componentDidMount: ->
    {dispatch, params} = @props
    page = parseInt(params.page, 10)
    if _.isNaN(page)
      dispatch(push('/not-found'))
    else
      dispatch(fetchAgents())
      offset = ((params.page - 2) * LIMIT) + DASHBOARD_LIMIT
      # We ask for one more to see if we need a paging link
      limit = LIMIT + 1
      dispatch(fetchLogs(offset, limit))

  # coffeelint: disable=max_line_length
  render: ->
    {agents, logs, params, agentsError, logsError} = @props

    page = parseInt(params.page, 10)

    <div className="row app-row-bg">
      <div className="page">
        {if agentsError?
          <ErrorBar err={agentsError} />
        }
        {if logsError?
          <ErrorBar err={logsError} />
        }
        <LogList logs={logs?.slice(0, LIMIT)} agents={agents} />
        {if (logs? and logs.length > LIMIT)
          <p>
            <Link id="view-more-logs" className="btn btn__regular" to="/logs/#{page+1}">More...</Link>
          </p>
        }
      </div>
    </div>

mapStateToProps = (state) ->
  agents: state.agent.items
  agentsError: state.agent.error
  logs: state.logs.logs
  logsError: state.logs.error

module.exports = connect(mapStateToProps)(MoreLogs)
