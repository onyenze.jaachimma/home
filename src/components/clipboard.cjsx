React = require 'react'

module.exports = React.createClass
  displayName: 'Clipboard'

  componentDidMount: ->
    if document
      clipboard = require 'clipboard'
      clipboard = new clipboard?(document.querySelectorAll('.copy-paste'))
      return

  # coffeelint: disable=max_line_length
  render: ->
    { copyText } = @props
    <span className="num dashboard-api__key spaced-medium">{copyText}
      <a onClick={(e) -> e.preventDefault()} className="copy-paste" data-clipboard-text={copyText}>
        <svg data-name="Layer 1" xmlns="http:#www.w3.org/2000/svg" viewBox="0 0 288 384" className="copy-paste__svg">
          <path d="M32 352V48H0v336h240v-32H32"/>
          <path d="M213.3 0H48v336h240V75zM256 112h-80V32h16v64h64v16z"/>
        </svg>
      </a>
    </span>
