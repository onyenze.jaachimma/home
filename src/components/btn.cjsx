React = require 'react'
{ Link } = require 'react-router'

module.exports = React.createClass
  displayName: "Btn"

  render:->
    <div>
     <Link to={@props.link} className={@props.class}>{@props.text}</Link>
    </div>
