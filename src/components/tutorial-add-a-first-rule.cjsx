React = require 'react'
{ Link } = require 'react-router'
{ connect } = require 'react-redux'
{ push } = require 'react-router-redux'
{ addRule, storeAgent, fetchAgent } = require '../actions/agent'
{ updateTutorial } = require '../actions/tutorial'


Checklist = require './tutorial-checklist'
EditRules = require './agents-edit-rules'

TutorialCreateAnAgent = React.createClass
  displayName: "Tutorial - Create an Agent"

  getInitialState: ->
    tutContentOpen: false
    ruleType: ''
    rule:
      weight: 1.0

  componentDidMount: ->
    { dispatch } = @props
    data =
      step: 'add-a-first-rule'
      agentID: @props.tutorial?.agentID
    dispatch updateTutorial(data)

    if not @props.agent?.id and @props.tutorial?.agentID
      dispatch fetchAgent(@props.tutorial.agentID)
    else if not @props.tutorial?.agentID
      dispatch push('/tutorial/create-an-agent')


    tutToggle = document.querySelectorAll('p.tut-toggle')
    tutExCont = document.querySelectorAll('.tut-extra-content')

    reset = ->
      tutExCont.forEach (content) ->
        if content.classList.contains('not-hidden')
          content.classList.remove 'not-hidden'
          content.classList.add 'hidden'
        return
      return

    openUp = (e) ->
      el = e?.currentTarget
      target = el?.nextElementSibling
      if target?.classList.contains('hidden')
        reset()
        el.classList.add 'tut-toggle-on'
        target.classList.remove 'hidden'
        target.classList.add 'not-hidden'
      else
        el.classList.remove 'tut-toggle-on'
        target.classList.add 'hidden'
        target.classList.remove 'not-hidden'
      return

    tutToggle.forEach (toggle) ->
      toggle?.addEventListener 'click', openUp, false
      return


  handleTypeChange: (value) ->
    @setState ruleType: value
    { rule } = @state
    rule.type = value
    @setState rule: rule

  addNewRule: (agent, rule) ->
    { dispatch } = @props
    dispatch(addRule(agent, rule))
    @setState @getInitialState()

  # coffeelint: disable=max_line_length
  render: ->
    { ruleType, rule } = @state
    { agent } = @props

    <div className="page">
      <div className="tutorial">
        <div className="tutorial__intro-text">
          <h1>Add your first rule</h1>
          <p>Fuzzy.ai agents start with rules. Here&apos;s how you can add a rule that says we should charge more when the temperature is higher</p>
        </div>
        <div className="tutorial__row">
        <div className="tutorial__text lined-steps">
          <p className="tut-toggle"><span className="num tut-step">01.</span> Click the <strong>Add New Rule</strong> button </p>
          <div className="tut-extra-content hidden">
            <p>For each agent, you need to add one or more rules to define how the agent should make decisions.</p>
          </div>
          <p className="tut-toggle"><span className="num tut-step">02.</span> Click on <strong>Increases</strong></p>
            <div className="tut-extra-content hidden">
              <h3 className="upper fwb mbr">There are <strong>three</strong> main kinds of rules:</h3>
              <ul>
                <li><b>Increases</b> &ndash; These are for when an input increases an output</li>
                <li><b>Decreases</b> &ndash; These are for when an input decreases an output</li>
                <li><b>If-Then</b> &ndash;  These are for more complicated situations</li>
              </ul>
            </div>
            <p className="tut-toggle"><span className="num tut-step">03.</span> In the <strong>Name of input</strong> field, enter <i>&ldquo;Temperature&rdquo;</i></p>
              <div className="tut-extra-content hidden">
                <p>This rule will cause <em>the price of a cup of lemonade</em> to increase as the temperature outside increases.</p>
              </div>
            <p className="tut-toggle"><span className="num tut-step">04.</span> In the <strong>From</strong> field, enter 32, and in the <strong>To</strong> field, enter 100</p>
            <div className="tut-extra-content hidden">
              <p>This means that as the temperature moves from 32° to 100°, the price will increase with it.</p>
            </div>
              <p className="tut-toggle"><span className="num tut-step">05.</span> Leave the <strong>Weight</strong> field set to 1</p>
              <div className="tut-extra-content hidden">
                <p>In this case because we only have one rule, the weight isn&apos;t relevant, but when you have many rules, you can use different weights on each rule. This will cause some rules to have a greater impact on the results than others.</p>
              </div>
            <p className="tut-toggle"><span className="num tut-step">06.</span> Click the <strong>Save</strong> button</p>
            <div className="tut-extra-content hidden">
              <p>Before moving on to the next step, you might also want to create a second rule. One idea could be to create an Increases rule called “Number of people in line” which causes the price to increase as more people are waiting in line for a cup.</p>
            </div>
            <p><span className="num tut-step">07.</span>Click the <strong>Go to Step Four</strong> button below to go to the next step</p>
          </div>
          <figure className="tutorial__image  f-right">
            <EditRules />
          </figure>
        </div>
        <div className="pagination tuts-pagination">
          <Link to="/tutorial/test-your-agent" className="btn btn-blue">Go to step four: <strong>test your agent</strong></Link>
          <br />
          <Link to="/tutorial/create-an-agent" className="secondary-tut-link previous-page-link">Go to previous page</Link>
          <Link to="/agents/new" className="secondary-tut-link">Skip tutorial</Link>
        </div>
      </div>
    </div>


mapStateToProps = (state) ->
  agent: state.agent.current
  tutorial: state.tutorial

module.exports = connect(mapStateToProps)(TutorialCreateAnAgent)
