React = require 'react'
_ = require 'lodash'

{ Link } = require 'react-router'
{sigfigs, fmt} = require '../sigfigs'

Log = React.createClass

  displayName: 'Log'

  # coffeelint: disable=max_line_length
  render: ->
    log = @props
    # blech
    agent = log.agent
    sig = sigfigs log.input

    <tr>
      <td data-title="Time">
        <Link to="/log/#{log.reqID}" className="log-time">{(new Date(log.createdAt)).toLocaleString()}</Link>
      </td>
      <td data-title="Agent">
        {if agent?
          <Link to="/agents/#{agent.id}" className="table-link-to-agent">{agent.name or "[Untitled]"}</Link>
        else
          <span>{log.agentID}</span>
        }
      </td>
      <td data-title="Input">
        <ul className="compressed-list data-list">
        { _.map log.input, (value, name) ->
          <li key={name} className="compressed-list__item"><span className="upper sup-heading fwb line">{name}:</span> <span className="num line">{value}</span></li>
        }
        </ul>
      </td>
      <td data-title="Output">
        <ul className="compressed-list data-list">
        { _.map _.omit(log.crisp, "meta"), (value, name) ->
          <li key={name} className="compressed-list__item"><span className="upper sup-heading fwb line">{name}:</span> <span className="num line">{fmt(value, sig)}</span></li>
        }
        </ul>
      </td>
    </tr>

module.exports = Log
