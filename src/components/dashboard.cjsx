_ = require 'lodash'
React = require 'react'
{ Link } = require 'react-router'
{ connect } = require 'react-redux'

DashboardApiKey = require './dashboard-api-key'
DashboardApiStats = require './dashboard-api-stats'
DashboardCurrentPlan = require './dashboard-current-plan'

DashboardSidebar = require './dashboard-sidebar'
APIUsageChart = require './api-usage-chart'
LogList = require './log-list'
UpgradePrompt = require './upgrade-prompt'
ProgressBar = require './progress-bar'
ErrorBar = require './error-bar'

TutorialStepBar = require './tutorial-steps-bar'


{ fetchAgents } = require '../actions/agent'
{ fetchLogs } = require '../actions/logs'
{ fetchPlan } = require '../actions/plans'
{ fetchStats } = require '../actions/stats'

OFFSET = 0
LIMIT = 5

class Dashboard extends React.Component
  displayName: 'Dashboard'

  componentDidMount: ->
    { dispatch, user } = @props
    dispatch(fetchAgents())
    if user?
      dispatch(fetchPlan(user.plan or "free"))
    dispatch(fetchLogs(OFFSET, LIMIT + 1))
    now = new Date()
    year = now.getUTCFullYear()
    month = now.getUTCMonth() + 1
    dispatch(fetchStats(year, month))

    agent_list = {}

  previousMonth: =>

    {dispatch, year, month} = @props

    if month == 1
      previousYear = year - 1
      previousMonth = 12
    else
      previousYear = year
      previousMonth = month - 1

    dispatch(fetchStats(previousYear, previousMonth))

    false

  nextMonth: =>

    {dispatch, year, month} = @props

    if month == 12
      nextYear = year + 1
      nextMonth = 1
    else
      nextYear = year
      nextMonth = month + 1

    dispatch(fetchStats(nextYear, nextMonth))

    false

  # coffeelint: disable=max_line_length
  render: ->
    {agents, logs, plan, byDay, total, year, month, user} = @props
    {agentInProgress, userInProgress, logsInProgress, statsInProgress, plansInProgress} = @props
    inProgress = agentInProgress || userInProgress || logsInProgress || statsInProgress || plansInProgress
    {agentError, userError, logsError, statsError, plansError} = @props
    { step, completed } = @props.tutorial


    now = new Date()
    thisYear = now.getUTCFullYear()
    monthNames = [
      "January"
      "February"
      "March"
      "April"
      "May"
      "June"
      "July"
      "August"
      "September"
      "October"
      "November"
      "December"
    ]
    thisMonth = now.getUTCMonth() + 1
    signup = new Date(user?.createdAt)
    signupYear = signup.getUTCFullYear()
    signupMonth = signup.getUTCMonth() + 1

    monthName = monthNames[month - 1]

    <div id="site">
      <div className="outer_wrap">
        <TutorialStepBar step={step} completed={completed} />

        <div className="dashboard">
        {if agentError?
          <ErrorBar err={agentError} />
        }
        {if userError?
          <ErrorBar err={userError} />
        }
        {if logsError?
          <ErrorBar err={logsError} />
        }
        {if statsError?
          <ErrorBar err={statsError} />
        }
        {if plansError?
          <ErrorBar err={plansError} />
        }
          <ProgressBar inProgress={inProgress} />
          <UpgradePrompt plan={plan} month={month} />
          <DashboardSidebar
            href = "/agents/new"
          />

          <section className="dashboard__main-zone">
            <div className="dashboard__content">
              <div className="dashboard__main-column">
                <div className="dashboard-top-section">

                  <DashboardApiKey />
                  <DashboardApiStats />
                  <DashboardCurrentPlan />

                </div>
                <div className="arrow-nav-wrapper arrow-nav">
                  <div className="arrow-nav-wrapper-inner">
                    <span className="sup-heading line upper fwb mbst txtC">API Usage for</span>
                    <h3 className="dash-modules__data mbst txtC">{monthName} {year}</h3>

                      <a href="#" onClick={@previousMonth} className="arrow-nav__prev-arrow prev-month arrow">
                        <span>
                          <svg viewBox="0 0 32 32" className="ui-icon left-arrow">
                            <use xlinkHref="/assets/sprite.svg#icon-chevron-left"></use>
                          </svg>
                        </span>
                      </a>

                    { if year < thisYear or (year == thisYear and month < thisMonth)
                      <a href="#" onClick={@nextMonth} className="arrow-nav__next-arrow next-month arrow">
                        <span>
                          <svg viewBox="0 0 32 32" className="ui-icon right-arrow">
                            <use xlinkHref="/assets/sprite.svg#icon-chevron-right"></use>
                          </svg>
                        </span>
                      </a>
                    }
                  </div>
                </div>
                <div className="dashboard__canvas-container aspect-ratio">
                  <APIUsageChart byDay={byDay}/>
                </div>
                <div className="dashboard__recent-eval dashboard__sub-sections" id="dashboard-log">
                  <h3 className="app-heading h2 fwb">Agent logs</h3>
                  <LogList logs={logs?.slice(OFFSET, LIMIT)} agents={agents} />
                  { if (logs? and logs.length > LIMIT)
                    <p>
                      <Link id="view-more-logs" className="btn btn__regular" to="/logs/2">View More</Link>
                    </p>
                  }
                </div>
              </div>
            </div>
          </section>
        </div>
      </div>
    </div>

mapStateToProps = (state) ->
  agentInProgress: state.agent.inProgress
  userInProgress: state.user.inProgress
  logsInProgress: state.logs.inProgress
  statsInProgress: state.stats.inProgress
  plansInProgress: state.plans.inProgress
  agentError: state.agent.error
  userError: state.user.error
  logsError: state.logs.error
  statsError: state.stats.error
  plansError: state.plans.error
  agents: state.agent.items
  user: state.user.account
  authenticated: state.user.authenticated
  logs: state.logs.logs
  plan: state.plans.current
  year: state.stats.year
  month: state.stats.month
  total: state.stats.total
  byDay: state.stats.byDay
  tutorial: state.tutorial

module.exports = connect(mapStateToProps)(Dashboard)
