React = require 'react'
{ Link } = require 'react-router'

ChecklistStep = React.createClass

  # coffeelint: disable=max_line_length
  render: ->
    { step, current } = @props
    if step.step < current.step
      classList = "checked tutorial-checklist__item"
    else
      classList = "unchecked tutorial-checklist__item"
    <li className={classList}>
      <span className="check-mark">
        <svg viewBox="0 0 32 32">
          <use xlinkHref="/assets/sprite.svg#icon-check"></use>
        </svg>
      </span>
      <span>{step.title}</span></li>

module.exports = React.createClass
  displayName: "Checklist"

  # coffeelint: disable=max_line_length
  render: ->
    current = @props.current
    <div className="tutorial-checklist-wrapper">
      <h2 className="checklist-heading fwb">Checklist</h2>
      <ul className="tutorial-checklist">
      {@props.steps.map (step, i) ->
        <ChecklistStep key={i} step={step} current={current} />
      }
      </ul>
    </div>
