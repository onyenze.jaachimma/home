React = require 'react'
{ Link } = require 'react-router'

module.exports = React.createClass
  displayName: "Docs"

  # coffeelint: disable=max_line_length
  render: ->
    <div>
      <h1>About RESTful APIs</h1>
      <p>A RESTful API lets you make HTTP requests to special URLs at fuzzy.ai. These requests use different HTTP &nbsp;<em>verbs</em> like GET, PUT, POST, and DELETE.</p>
      <p>If you&rsquo;re unfamiliar with RESTful APIs, the excellent book <a href="http://shop.oreilly.com/product/9780596529260.do">RESTful Web Services</a> is highly recommended.</p>
      <p>For fuzzy.ai, all API requests go to the <a href="https://api.fuzzy.ai/">https://api.fuzzy.ai/</a> server. All API requests are encrypted with SSL.</p>
    </div>
