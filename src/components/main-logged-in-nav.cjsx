React = require 'react'
{ Link } = require 'react-router'


module.exports = React.createClass
  displayName: "Main logged In Nav"

  render: ->
    step = @props.tutorial
    <ul className="navigation dashboard-nav">
      <li className="navigation__item">
        <Link to="/" className="navigation__link navigation__link__active">Dashboard</Link>
      </li>
      <li className="navigation__item">
        <Link to="/docs" className="navigation__link">Docs</Link>
      </li>

      <li className="navigation__item">
        <a href="http://blog.fuzzy.ai/" className="navigation__link">Blog</a>
      </li>
      {if step?
        <li className="navigation__item">
          <a href="/tutorial/#{step}" className="navigation__link">Finish Tutorial</a>
        </li>
      }
    </ul>
