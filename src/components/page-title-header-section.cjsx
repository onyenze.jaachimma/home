React = require 'react'
{ Link } = require 'react-router'

module.exports = React.createClass

  displayName: "page title"

  render: ->
    <div className="row-inner-section-hero">
      <main className="page-intro-zone page">
        <header className="main-intro-text">
          <h1 className="page-title-wrap__heading inline big-title fwb">{@props.title}</h1>
          <h2 className="big-title light-color inline fwl"> {@props.subtitle}</h2>
          <p className="big-para">{@props.desc1}</p>
          {if @props.desc2
            <p className="big-para">{@props.desc2}</p>
          }
          {if @props.desc3
            <p className="big-para">{@props.desc3}</p>
          }
        </header>
      </main>
    </div>
