React = require 'react'
{ Link } = require 'react-router'

module.exports = React.createClass
  displayName: 'PasswordChanged'

  # coffeelint: disable=max_line_length
  render: ->
    <div className="row">
      <div className="page">
        <h1>Password changed.</h1>
        <p>Your password has been updated. Use it next time you log in.</p>
        <p><Link to="/">Go to your dashboard</Link></p>
      </div>
    </div>
