React = require 'react'

module.exports = React.createClass
  displayName: 'ChangePasswordSettings'

  getInitialState: ->
    errors: {}

  validate: (values) ->
    errors = {}
    if (!values.newPassword)
      errors.newPassword = "Password required."

    if (values.newPassword.length < 8)
      errors.newPassword = "Password must be 8 characters or more."

    if (values.newPasswordConfirm != values.newPassword)
      errors.newPasswordConfirm = "Password and confirmation must match."

    if Object.keys(errors).length
      @setState errors: errors
      false
    else
      true

  handleSubmit: (e) ->
    e.preventDefault()
    {submitHandler} = @props

    data =
      oldPassword: @refs.old_password.value
      newPassword: @refs.new_password.value
      newPasswordConfirm: @refs.new_password_confirm.value

    if @validate data
      @setState errors: {}
      submitHandler(data)

  # coffeelint: disable=max_line_length
  render: ->
    { errors } = @state
    { user } = @props
    <div className="module module-change-password">
      <h2>Change Password</h2>
      <form onSubmit={@handleSubmit} className="user-forms">
        <div className="form-group input-wrap">
          <label className="upper spaced-medium" htmlFor="password">Current Password</label>
          <input className="form-control" ref="old_password"  placeholder="Current Password" type="text" />
        </div>
        <div className="form-group input-wrap">
          <label className="upper spaced-medium" htmlFor="password">New Password</label>
          <input className="form-control"  ref="new_password" placeholder="New Password" required="required" type="password" />
          { if errors?.newPassword?
            <label className="error" htmlFor="new_password">{ errors.newPassword }</label>
          }
        </div>
        <div className="form-group input-wrap">
          <label className="upper spaced-medium" htmlFor="password">Retype New Password</label>
          <input className="form-control" ref="new_password_confirm" placeholder="Retype New Password" required="required" type="password" />
          { if errors?.newPasswordConfirm?
            <label className="error" htmlFor="new_password">{ errors.newPasswordConfirm }</label>
          }
        </div>
        <button className="btn">Update</button>
      </form>
    </div>
