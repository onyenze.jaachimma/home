React = require 'react'
{ browserHistory, Link } = require 'react-router'
{ connect } = require 'react-redux'

{ signup, reset, resendConfirmation } = require '../actions/user'

Signup = React.createClass
  displayName: "Signup"

  getInitialState: ->
    errors: {}
    tosChecked: false
    code: ''

  componentDidMount: ->
    { dispatch, location } = @props
    dispatch(reset())
    if location.query?.code
      @setState code: location.query?.code


  handleTOSChange: ->
    @setState tosChecked: @refs.tos.checked

  handleCodeChange: ->
    @setState code: @refs.code.value

  validate: (values) ->
    errors = {}

    if (!values.email)
      errors.email = "Email address required."

    if (!values.password)
      errors.password = "Password required."

    if (values.password.length < 8)
      errors.password = "Password must be 8 characters or more."

    if (values.confirm != values.password)
      errors.confirm = "Password and confirmation must match."

    if (!values.tos)
      errors.tos = "You must accept the terms of service."

    if Object.keys(errors).length
      @setState errors: errors
      false
    else
      true

  handleSubmit: (e) ->
    e.preventDefault()

    {dispatch} = @props

    values =
      email: @refs.email.value.trim()
      password: @refs.password.value.trim()
      confirm: @refs.confirm.value.trim()
      code: @refs.code.value.trim()
      tos: @refs.tos.checked
      subscribe: @refs.subscribe.checked

    if @validate values
      dispatch(signup(values))

  handleReConfirm: (e) ->
    e.preventDefault()

    email = @refs.email.value

    {dispatch} = @props
    dispatch(resendConfirmation({email: email}))

  # coffeelint: disable=max_line_length
  render: ->
    { errors, code, tosChecked } = @state
    { inProgress, error, needsReConfirm } = @props.user

    <div className="row">
      <div className="page">
        <p id="form-parent"></p>
        <div className="form-wrapper">
          <div className="form_text">
            <h1 className="page_title">Sign up</h1>
            <Link to="/login">Already have an account?</Link>
            {if error
              <p className="error_message">
                { error }
                { if needsReConfirm
                  <a href="#" className="design-TODO" onClick={@handleReConfirm}> Resend confirmation email.</a>
                }
              </p>
            }
          </div>
          <form action="#" id="signup" method="post" name="signup" role="form" className="user-forms" onSubmit={@handleSubmit}>
            <fieldset>
              <div className="form-group input-wrap">
                <label className="upper spaced-medium" htmlFor="email">Email address</label>
                <input className="form-control " id="email" ref="email" name="email" placeholder="Enter email" required="required" type="email" />
                { if errors?.email?
                  <label className="error" htmlFor="email">{ errors.email }</label>
                }
              </div>
              <div className="form-group input-wrap">
                <label className="upper spaced-medium" htmlFor="password">Password</label>
                <input className="form-control " id="password" ref="password" name="password" className="error_input" placeholder="Password" required="required" type="password" />
                { if errors?.password?
                  <label className="error" htmlFor="password">{ errors.password }</label>
                }
              </div>
              <div className="form-group input-wrap">
                <label className="upper spaced-medium" htmlFor="password">Confirm</label>
                <input className="form-control " id="confirm" ref="confirm" name="confirm" placeholder="Re-type password" required="required" type="password" />
                { if errors?.confirm?
                  <label className="error" htmlFor="confirm">{ errors.confirm }</label>
                }
              </div>
              <div className="form-group input-wrap input-wrap-last">
                <label className="upper spaced-medium" htmlFor="code">COUPON CODE <small>Optional</small></label>
                <input className="form-control input-half size1of2 unit" id="code" ref="code" name="code" value={code} onChange={@handleCodeChange} placeholder="Enter coupon code if you have one" type="text" />
                <label className="error" htmlFor="code"></label>
                <br />
              </div>
              <div className="checkbox input-wrap">
                <input name="tos" ref="tos" id="tos" required="required" title="You must accept the Terms of Service." type="checkbox" checked={tosChecked} onChange={@handleTOSChange}/>
                <label htmlFor="tos">I have read and agree to the fuzzy.ai <Link to="/tos">Terms of Service</Link>.</label>
                { if errors?.tos?
                  <label className="error" htmlFor="tos">{ errors.tos }</label>
                }
              </div>
              <div className="checkbox input-wrap">
                <input defaultChecked={true} name="subscribe" ref="subscribe" type="checkbox" id="subscribe" />
                <label htmlFor="subscribe">Send me occasional updates on the service by email.</label>
              </div>
              { if inProgress
                <button className="btn btn-default" disabled="disabled">Working...</button>
              else
                <button className="btn btn__regular" type="submit">Signup</button>
              }
            </fieldset>
          </form>
        </div>
      </div>
    </div>

mapStateToProps = (state) ->
  user: state.user

module.exports = connect(mapStateToProps)(Signup)
