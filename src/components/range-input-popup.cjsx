React = require 'react'

RangeInput = require './range-input'
AgentsEditAdvanced = require './agents-edit-advanced'
{ GearIcon } = require './icons'
{ DeleteCross } = require './icons'

RangeInputPopup = React.createClass
  displayName: 'RangeInputPopup'

  getInitialState: ->
    showInputRange: false
    showAdvanced: false
    inputRange: @_determineInputRange(@props.input) or [0, 100]
    inputSets: @props.input or null

  componentWillReceiveProps: (nextProps) ->
    if nextProps.title != @props.title
      range = @_determineInputRange(nextProps.input)
      @setState inputRange: range or [0, 100]

  handleRange: (range) ->
    @setState inputRange: range
    @props.saveHandler(range)

  handleSets: (sets) ->
    @setState inputSets: sets
    @props.saveHandler(sets)

  toggleAdvancedEdit: (e) ->
    e.preventDefault()
    @setState showAdvanced: not @state.showAdvanced

  toggleRange: (e) ->
    e.preventDefault()
    @setState showInputSettings: true

  _handleAdvancedKeyDown: (e) ->
    if e.keyCode == 27
      @setState showAdvanced: false

  _determineInputRange: (input) ->
    if input
      values = _.flatten _.values _.values(input)
      if values.length
        range = [_.min(values), _.max(values)]


  handleAdvanceEditClose: ->
    @setState showAdvanced: not @state.showAdvanced

  # coffeelint: disable=max_line_length
  render: ->
    {showInputRange, showAdvanced} = @state
    masthead = document.querySelector('.masthead')
    noEdits = document.querySelectorAll('.no-edit')
    saveBtn = document.querySelector('.btn__regular')
    agentNewRuleZone = document.querySelector('.agent__new-rule')

    if showAdvanced
      advanceRangeClass = "advance-range_active advance-range"
      handleAdvanceEdit = @handleAdvanceEditClose
      masthead.classList.add 'masthead-hide'
      agentNewRuleZone.classList.add 'agent__new-rule-hide'

      noEdits.forEach (noEdit) ->
        noEdit.classList.add 'no-edit-hide'
        return

    else
      advanceRangeClass = "advance-range_not-active advance-range blue"
      handleAdvanceEdit = @handleAdvanceEditClose
      masthead.classList.remove 'masthead-hide'
      agentNewRuleZone.classList.remove 'agent__new-rule-hide'




    <div className="rule-settings-wrap" onKeyDown={@handleEscape}>

      <div className="rule-settings">
        <span className="heading-text spaced-small h5 upper fwn">Set The range for</span>
        <h2 className="upper fwb mbst spaced-small h4">{@props.title}</h2>
        <div className="rule-range-input-wrap">
          <RangeInput handleRange={@handleRange} fromValue={@state.inputRange[0]} toValue={@state.inputRange[1]}  />

          {if @state.inputSets
            <a href="#" onClick={@toggleAdvancedEdit} className="fwb spaced-small advance-link"><span><GearIcon /></span>Advanced Settings</a>
          }
        </div>
      </div>


      {if showAdvanced
        console.log @state.inputSets
        <div className={advanceRangeClass} onKeyDown={@_handleAdvancedKeyDown}>
          <span className="close-advance-range" onClick={handleAdvanceEdit}>
          <DeleteCross />
          </span>
          <AgentsEditAdvanced input={@props.input} handleSets={@handleSets} />
        </div>
      }

    </div>

module.exports = RangeInputPopup
