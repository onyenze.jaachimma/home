# src/reducers/stats.coffee
# Copyright 2016 9165584 Canada Corporation <legal@fuzzy.ai>
# All rights reserved.

initialState =
  inProgress: false
  year: null
  month: null
  agentID: null
  total: null
  byDay: null
  error: null

module.exports = (state = initialState, action) ->
  newState = switch (action.type)
    when 'REQUEST_STATS'
      inProgress: true
      year: action.year
      month: action.month
      agentID: null
      total: null
      byDay: null
      error: null
    when 'REQUEST_AGENT_STATS'
      inProgress: true
      year: action.year
      month: action.month
      agentID: action.agentID
      total: null
      byDay: null
      error: null
    when 'RECEIVE_STATS'
      inProgress: false
      year: action.year
      month: action.month
      total: action.total
      byDay: action.byDay
      agentID: null
      error: null
    when 'RECEIVE_AGENT_STATS'
      inProgress: false
      year: action.year
      month: action.month
      total: action.total
      byDay: action.byDay
      agentID: action.agentID
      error: null
    when 'STATS_ERROR'
      inProgress: false
      year: action.year
      month: action.month
      error: action.error
      total: null
      byDay: null
      agentID: null
    when 'AGENT_STATS_ERROR'
      inProgress: false
      year: action.year
      month: action.month
      error: action.error
      agentID: action.agentID
      total: null
      byDay: null

  if newState?
    _.assign {}, state, newState
  else
    state
