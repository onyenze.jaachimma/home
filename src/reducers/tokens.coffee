# src/reducers/tokens.coffee
# Copyright 2016 9165584 Canada Corporation <legal@fuzzy.ai>
# All rights reserved.

_ = require 'lodash'

initialState =
  inProgress: false
  items: []
  error: null

module.exports = (state = initialState, action) ->
  newState = switch (action?.type)
    when 'REQUEST_TOKENS'
      inProgress: true
    when 'RECEIVE_TOKENS'
      inProgress: false
      items: action.tokens
      error: null
    when 'RECEIVE_TOKENS_ERROR'
      inProgress: false
      error: action.error
    when 'ADD_TOKEN'
      inProgress: true
    when 'ADD_TOKEN_SUCCESS'
      inProgress: false
      items: state.items.concat [action.token]
      error: null
    when 'ADD_TOKEN_ERROR'
      inProgress: false
      error: action.error

  if newState?
    _.assign {}, state, newState
  else
    state
