# src/reducers/plans.coffee
# Copyright 2016 9165584 Canada Corporation <legal@fuzzy.ai>
# All rights reserved.

_ = require 'lodash'

initialState =
  inProgress: false
  items: []
  current: null
  error: null

module.exports = (state = initialState, action) ->
  newState = switch (action.type)
    when 'REQUEST_PLANS'
      inProgress: true
    when 'RECEIVE_PLANS'
      inProgress: false
      items: action.plans
    when 'REQUEST_PLANS_ERROR'
      inProgress: false
      items: []
      error: action.error
    when 'REQUEST_PLAN'
      inProgress: true
    when 'RECEIVE_PLAN'
      inProgress: false
      current: action.plan
    when 'REQUEST_PLAN_ERROR'
      inProgress: false
      current: null
      error: action.error

  if newState?
    _.assign {}, state, newState
  else
    state
