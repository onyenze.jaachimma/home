# paymentsclient.coffee
# Copyright 2014-2016 9165584 Canada Corporation <legal@fuzzy.ai>
# All rights reserved.

MicroserviceClient = require '@fuzzy-ai/microservice-client'

class PaymentsClient extends MicroserviceClient

  setCustomer: (data, callback) ->
    @post "/customers", data, callback

  getCustomer: (customerId, callback) ->
    @get "/customers/#{customerId}", callback

  updateCustomer: (customerId, data, callback) ->
    @put "/customers/#{customerId}", data, callback

  getInvoices: (customerId, callback) ->
    @get "/customers/#{customerId}/invoices", callback

  getInvoice: (invoiceId, callback) ->
    @get "/invoices/#{invoiceId}", callback

module.exports = PaymentsClient
