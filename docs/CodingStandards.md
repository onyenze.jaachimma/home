# Fuzzy.ai Style Guide

## .cjsx files

JSX (render results) should contain:

* 2 space indentation
* no blank lines

Example:

      class MyComponent
        render: ->
          <div>
            <div className="example"></div>
          </div>

BEM front-end

* Block
* Element
* Modifier

Example:

      <ul className="list">  block element:: represents the higher level of an abstraction or module
        <li className="list__item">  element uses double underscore it represents a descendent of list
        <li className="list__item--last">  last is the modifier double hyphen
      </ul>
