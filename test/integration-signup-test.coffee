# integration-login-test.coffee
# Copyright 2016 9165584 Canada Corporation <legal@fuzzy.ai>
# All rights reserved.

vows = require 'perjury'
assert = vows.assert

{ browser, apiBatch } = require './apibatch'

vows
  .describe("Successful /signup")
  .addBatch apiBatch
    'and we visit /signup':
      topic: ->
        browser.visit '/signup', @callback
        undefined
      'it works': ->
        browser.assert.success()
      'it has an email field': ->
        browser.assert.input '#email', ''
      'it has a password field': ->
        browser.assert.input '#password', ''
      'it has a password confirmation field': ->
        browser.assert.input '#confirm', ''
      'it has a coupon code field': ->
        browser.assert.input '#code', ''
      'it has a ToS checkbox': ->
        browser.assert.input '#tos', ''
      'it has a "signup" button': ->
        browser.assert.text 'button.btn', 'Signup'
      'and we signup':
        topic: ->
          browser.fill 'email', 'unittest@example.com'
            .fill 'password', 'unit-test-password'
            .fill 'confirm', 'unit-test-password'
            .check '#tos'
            .pressButton 'Signup', @callback
          undefined
        'it works': ->
          browser.assert.success()
        'it has a dashboard element': ->
          browser.assert.url '/signup-complete'
  .export(module)
