# integration-login-test.coffee
# Copyright 2016 9165584 Canada Corporation <legal@fuzzy.ai>
# All rights reserved.

vows = require 'perjury'
assert = vows.assert

{ browser, loginBatch } = require './apibatch'

vows
  .describe("GET /settings")
  .addBatch loginBatch
    'and we visit /settings':
      topic: ->
        browser.visit '/settings', @callback
        undefined
      'it works': ->
        browser.assert.success()
      'it has a bunch of forms': ->
        browser.assert.elements('.btn', {atLeast: 4})
  .export(module)
