# integration-get-about-test.coffee
# Copyright 2016 9165584 Canada Corporation <legal@fuzzy.ai>
# All rights reserved.

vows = require 'perjury'
assert = vows.assert

{ browser, apiBatch } = require './apibatch'

vows
  .describe("GET /about")
  .addBatch apiBatch
    'and we visit /about':
      topic: ->
        browser.visit '/about', @callback
        undefined
      'it works': ->
        browser.assert.success()
      'it has the full team': ->
        browser.assert.elements('.team-member', {atLeast: 5})
  .export(module)
