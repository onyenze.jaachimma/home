# apibatch.coffee
# Copyright 2017 9165584 Canada Corporation <legal@fuzzy.ai>
# All rights reserved.

vows = require 'perjury'
assert = vows.assert
child_process = require 'child_process'
_ = require 'lodash'
path = require 'path'
Browser = require 'zombie'

env = require './env'

Browser.localhost('localhost', env['WEB_PORT'])

browser = new Browser()
browser.on 'error', (error) -> console.error error.stack

base =
  'When we run a webserver':
    topic: ->
      callback = @callback
      modulePath = path.join(__dirname, "..", "lib", "main.js")
      child = child_process.fork modulePath, [], {env: env, silent: true}

      child.once "error", (err) ->
        callback err

      child.stderr.on "data", (data) ->
        str = data.toString('utf8')
        process.stderr.write "SERVER ERROR: #{str}\n"

      child.stdout.on "data", (data) ->
        str = data.toString('utf8')
        if str.match "Server listening"
          callback null, child
        else if env.SILENT != "true"
          process.stdout.write "SERVER: #{str}\n"
      undefined
    'it works': (err, child) ->
      assert.ifError err
      assert.isObject child
    'teardown': (child) ->
      child.kill()

login =
  'and we visit /login':
    topic: ->
      browser.visit '/login', @callback
      undefined
    'it works': ->
      browser.assert.success()
    'and we login':
      topic: ->
        browser.fill 'email', 'test@example.com'
          .fill 'password', 'testing123'
          .pressButton 'Login', @callback
        undefined
      'it works': ->
        browser.assert.success()

exports.browser = browser

exports.apiBatch = (rest) ->
  batch = _.cloneDeep base
  a = 'When we run a webserver'
  _.assign(batch[a], rest)
  batch

exports.loginBatch = (rest) ->
  a = 'When we run a webserver'
  b = 'and we visit /login'
  c = 'and we login'
  batch = _.cloneDeep base
  l = _.cloneDeep login
  _.assign(batch[a], l)
  _.assign(batch[a][b][c], rest)
  batch
