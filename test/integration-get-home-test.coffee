# get-home-integration-test.coffee
# Copyright 2016 9165584 Canada Corporation <legal@fuzzy.ai>
# All rights reserved.

vows = require 'perjury'
assert = vows.assert

{ browser, apiBatch } = require './apibatch'

vows
  .describe("GET /")
  .addBatch apiBatch
    'and we visit /':
      topic: ->
        browser.visit '/', @callback
        undefined
      'it works': ->
        browser.assert.success()
      'it has a link to login': ->
        browser.assert.link('.login-nav a', 'Log in', '/login')
      'it has a link to signup': ->
        browser.assert.link('.login-nav a', 'Get Started', '/signup')
  .export(module)
