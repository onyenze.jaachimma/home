# integration-how-it-works-test.coffee
# Copyright 2016 9165584 Canada Corporation <legal@fuzzy.ai>
# All rights reserved.

vows = require 'perjury'
assert = vows.assert

{ browser, apiBatch } = require './apibatch'

vows
  .describe("GET /how-it-works")
  .addBatch apiBatch
    'and we visit /':
      topic: ->
        browser.visit '/', @callback
        undefined
      'it works': ->
        browser.assert.success()
      'and we click the "How it works" link':
        topic: ->
          browser.clickLink 'a[href="/how-it-works"]', @callback
          undefined
        'it works': ->
          browser.assert.success()
        'it has "How it works" in the header': ->
          browser.assert.text('h1', 'How it works.')
  .export(module)
