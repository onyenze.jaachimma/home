# integration-pricing-test.coffee
# Copyright 2016 9165584 Canada Corporation <legal@fuzzy.ai>
# All rights reserved.

vows = require 'perjury'
assert = vows.assert

{ browser, apiBatch } = require './apibatch'

vows
  .describe("GET /pricing")
  .addBatch apiBatch
    'and we visit /pricing':
      topic: ->
        browser.visit '/pricing', @callback
        undefined
      'it works': ->
        browser.assert.success()
      'it has a Free Plan': ->
        browser.assert.text('.pricing-zone h3', new RegExp('Free Plan.*'))
      'it has at least one other plan': ->
        browser.assert.elements('.pricing-package', {atLeast: 2})
      'it has links to signup': ->
        browser.assert.link('.btn', 'Get Started', '/signup')
  .export(module)
