# integration-request-test.coffee
# Copyright 2016 9165584 Canada Corporation <legal@fuzzy.ai>
# All rights reserved.

vows = require 'perjury'
assert = vows.assert

{ browser, apiBatch } = require './apibatch'

vows
  .describe("GET /request")
  .addBatch apiBatch
    'and we visit /request':
      topic: ->
        browser.visit '/request', @callback
        undefined
      'it works': ->
        browser.assert.redirected()
      'we are at /signup': ->
        browser.assert.url('/signup')
  .export(module)
